﻿using System;
using System.Collections.Generic;
using System.Text;


namespace ElectricFilter.WpfApp
{
    public class AgregatPitaniya
    {
        public int Id { get; set; }
        public string p_mark { get; set; }
        public int p_rectifiedMax { get; set; }
        public int p_rectifiedMin { get; set; }
        public int p_average_rectified_current { get; set; }
        public int p_voltage { get; set; }
        public int p_current { get; set; }
        public int p_power { get; set; }
        public double p_efficency { get; set; }
        public double p_power_co { get; set; }

    }
}
