﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElectricFilter.WpfApp
{
    public class IGAFilter
    {
        public int Id { get; set; }
        public string f_mark { get; set; }
        public double f_active_lenght { get; set; }
        public double f_square_active { get; set; }
        public double f_square_total { get; set; }
        public int[] f_overall_dim { get; set; }   

        public double NCK { get; set; }
        public double NG { get; set; }
        public double H { get; set; }
        public double N_osad { get; set; }
        public double N_el_pole { get; set; }
        public double t_rab { get; set; }
        public double P_rab { get; set; }

    }
}
