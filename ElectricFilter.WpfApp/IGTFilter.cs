﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElectricFilter.WpfApp
{
    public class IGTFilter
    {
        public int Id { get; set; }
        public string f_mark { get; set; }
        public int f_square_total { get; set; }
        public double LE { get; set; }
        public double BE { get; set; }
        public double HE { get; set; }
        public double mod_filtr { get; set; }
        public double N_polei_long { get; set; }
        public double L { get; set; }
        public double FA { get; set; }
    }
}
