﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xaml;
using System.Xaml.Schema;
using FilterFormules;
using System.IO;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static ElectricFilter.WpfApp.ReaultUC;





namespace ElectricFilter.WpfApp
{
    /// <summary>
    /// Логика взаимодействия для Ishodniki.xaml
    /// </summary>
    public partial class Ishodniki : UserControl
    {
        public Ishodniki()
        {
            InitializeComponent();

            GetDeafultValues();
        }

        public Formules FormSample;
        public ReaultUC RUC;


        // ВАЛИДАЦИЯ ДАННЫХ
        public int DS_Count(string s)
        {
            string substr = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString();
            int count = (s.Length - s.Replace(substr, "").Length) / substr.Length;
            return count;
        }

        public void TextboxValidation(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !((Char.IsDigit(e.Text, 0) || ((e.Text == System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString()) && (DS_Count(((TextBox)sender).Text) < 1))));
        }


        //public DataUC()
        //{
        //    InitializeComponent();

        //    foreach (TextBox t in Grid.Children.OfType<TextBox>())
        //    {
        //        t.PreviewTextInput += TextboxValidation;
        //    }

        //    Deserialize();

        //}

        public void Deserialize()
        {
            try
            {
                JsonSerializer formatter = new JsonSerializer();
                using (StreamReader fs = new StreamReader("InputData.txt"))
                {
                    Dictionary<string, object> Data = (Dictionary<string, object>)formatter.Deserialize(fs, typeof(Dictionary<string, object>));

                    TB_v_ro.Text = Convert.ToDouble(Data["v_ro"]).ToString();
                    TB_x.Text = Convert.ToDouble(Data["x"]).ToString();
                    TB_tr.Text = Convert.ToDouble(Data["tr"]).ToString();
                    TB_pbar.Text = Convert.ToDouble(Data["pbar"]).ToString();
                    TB_pgi.Text = Convert.ToDouble(Data["pgi"]).ToString();
                    TB_w.Text = Convert.ToDouble(Data["w"]).ToString();
                    TB_kpyli.Text = Convert.ToDouble(Data["kpyli"]).ToString();
                    TB_sr_tok.Text = Convert.ToDouble(Data["sr_tok"]).ToString();
                    TB_vz_gaza.Text = Convert.ToDouble(Data["vz_gaza"]).ToString();
                    TB_ud_sopr.Text = Convert.ToDouble(Data["ud_sopr"]).ToString();

                    foreach (double value in ((JArray)Data["spyli"]).ToObject<List<double>>())
                    {
                        TB_spyli.Text += value.ToString() + " ";
                    }

                    // Удаление последнего пробела из текстбокса
                    TB_spyli.Text = TB_spyli.Text.Substring(0, TB_spyli.Text.Length - 1);

                    TB_vz_gaza.Text = Convert.ToDouble(Data["D_V"]).ToString();
                    TB_ud_sopr.Text = Convert.ToDouble(Data["P_EL"]).ToString();
                }


            }
            catch
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<double> textBoxesValues = new List<double>();

            try
            {
                textBoxesValues.Add(Convert.ToDouble(TB_v_ro.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_x.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_tr.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_pbar.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_pgi.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_w.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_kpyli.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_sr_tok.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_spyli.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_vz_gaza.Text));
                textBoxesValues.Add(Convert.ToDouble(TB_ud_sopr.Text));
            }
            catch
            {
                if (textBoxesValues.Count == 0)
                {
                    GetDeafultValues(); // Вывод исходных значений при запуске первом запуску приложения (до формирования файла сериализации)
                }
            }


        }

        /// <summary>
        /// РАСЧЁТ
        /// </summary>
       

        public void Serialize()
        {
            FieldInfo[] fields = Formules.GetFieldInfo();

            Dictionary<string, object> Data = new Dictionary<string, object>();

            foreach (FieldInfo Info in fields)
            {
                Data.Add(Info.Name, Info.GetValue(FormSample));
            }

            JsonSerializer formatter = new JsonSerializer();
            using (StreamWriter fs = new StreamWriter("InputData.txt"))
            {
                formatter.Serialize(fs, Data);
            }
        }

        /// <summary>
        /// ПРИСВОЕНИЕ ПЕРЕМЕННЫМ КЛАССА ЗНАЧЕНИЙ ИЗ ФОРМ ВВОДА
        /// </summary>
        public void GetSourceValues()
        {
            // Присвоение переменным исходных значений соответствующих значений
            double v_ro = Convert.ToDouble(TB_v_ro.Text);
            double x = Convert.ToDouble(TB_x.Text);
            double tr = Convert.ToDouble(TB_tr.Text);
            double pbar = Convert.ToDouble(TB_pbar.Text);
            double pgi = Convert.ToDouble(TB_pgi.Text);
            double w = Convert.ToDouble(TB_w.Text);
            double kpyli = Convert.ToDouble(TB_kpyli.Text);
            double sr_tok = Convert.ToDouble(TB_sr_tok.Text);

            List<double> spyli = new List<double>();

            string[] filter = TB_spyli.Text.Split(new char[] { ' ' });
            foreach (string s in filter)
            {
                spyli.Add(Convert.ToDouble(s));
            }

            double vz_gaza = Convert.ToDouble(TB_vz_gaza.Text);
            double ud_sopr = Convert.ToDouble(TB_ud_sopr.Text);

            // Создание экземпляра класса
            FormSample = new Formules(v_ro, x, tr, pbar, pgi, w, spyli, sr_tok, kpyli, vz_gaza, ud_sopr);

        }

        /// <summary>
        /// ПОЛУЧЕНИЕ ИСХОДНЫХ ЗНАЧЕНИЙ (ДЛЯ КОНТРОЛЬНОГО ПРИМЕРА)
        /// </summary>
        public void GetDeafultValues()
        {

            TB_v_ro.Text = "108000";
            TB_x.Text = "100";
            TB_tr.Text = "300";
            TB_pbar.Text = "101,5";
            TB_pgi.Text = "9,5";
            TB_w.Text = "0,8";
            TB_sr_tok.Text = "0,22";
            TB_kpyli.Text = "30";
            TB_spyli.Text = "2 10 15 25 50 75";
            TB_vz_gaza.Text = Math.Pow(2.2, -5).ToString();
            TB_ud_sopr.Text = "100000000";
        }



        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            List<string> textBoxesValues = new List<string>();

            textBoxesValues.Add(TB_v_ro.Text);
            textBoxesValues.Add(TB_x.Text);
            textBoxesValues.Add(TB_tr.Text);
            textBoxesValues.Add(TB_pbar.Text);
            textBoxesValues.Add(TB_pgi.Text);
            textBoxesValues.Add(TB_w.Text);
            textBoxesValues.Add(TB_kpyli.Text);
            textBoxesValues.Add(TB_sr_tok.Text);
            textBoxesValues.Add(TB_spyli.Text);
            textBoxesValues.Add(TB_vz_gaza.Text);
            textBoxesValues.Add(TB_ud_sopr.Text);

            bool IsValuesCorrect = true;

            // Исключение "пустых" значений
            foreach (string s in textBoxesValues)
            {
                string[] filterEmpty = s.Split(new char[] { ' ' });
                int counter = 0;
                foreach (string n in filterEmpty)
                {
                    if (n == "")
                    {
                        counter++;
                    }
                }

                if (counter >= 1 || s

                == String.Empty || s == ",")
                {
                    MessageBox.Show("Поля не должны быть пустыми", "Предупреждение");
                    IsValuesCorrect = false;

                    counter = 0;
                    break;
                }


            }

            if (IsValuesCorrect == true)
            {

                // Получение значений
                GetSourceValues();

                Serialize();

                FormSample.GetAllValues();

                // Очистка коллекции исходных значений Фракционного состава пыли для возможности повторных расчётов
                FormSample.spyli.Clear();

                ReaultUC.FormSample = FormSample;
<<<<<<< HEAD
=======
                RUC = new ReaultUC();

>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538
                RUC.GetAssigningValues();


            }

        }
    }
}
