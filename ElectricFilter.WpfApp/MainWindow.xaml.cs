﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Xaml;
using System.Xaml.Schema;
using FilterFormules;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ElectricFilter.WpfApp;


namespace ElectricFilter.WpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DDD.RUC = UCR;
        }


        private void Close_DpiChanged(object sender, DpiChangedEventArgs e)
        {

        }

        private void Information_Click(object sender, RoutedEventArgs e)
        {
            //    DataUserControl.Visibility = Visibility.Hidden;
            DDD.Visibility = Visibility.Hidden;
            UCI.Visibility = Visibility.Visible;
            UCR.Visibility = Visibility.Hidden;

        }

        private void Data_Click(object sender, RoutedEventArgs e)
        {
            
            DDD.Visibility = Visibility.Visible;
            UCI.Visibility = Visibility.Hidden;
            UCR.Visibility = Visibility.Hidden;

        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void RichTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Results_Click(object sender, RoutedEventArgs e)
        {
           DDD.Visibility = Visibility.Hidden;
            UCI.Visibility = Visibility.Hidden;
            UCR.Visibility = Visibility.Visible;
        }

        private void UCI_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }
        
    }
}
