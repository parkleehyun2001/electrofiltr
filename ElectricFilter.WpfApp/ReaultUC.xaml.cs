﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FilterFormules;
using System.IO;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static ElectricFilter.WpfApp.Ishodniki;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Win32;
<<<<<<< HEAD
=======

>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538


namespace ElectricFilter.WpfApp
{
    /// <summary>
    /// Логика взаимодействия для ReaultUC.xaml
    /// </summary>
    public partial class ReaultUC : UserControl
    {
        public static Formules FormSample;
        public void ReportInitializate()
        {
            try
            {
                //GetSourceValues();

                Serialize();

                FormSample.GetAllValues();

                GetAssigningValues();

                // ReportInitializationClass.ReportСonfigure();
            }
            catch
            {

            }
        }

        // ВАЛИДАЦИЯ ДАННЫХ
        public int DS_Count(string s)
        {
            string substr = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString();
            int count = (s.Length - s.Replace(substr, "").Length) / substr.Length;
            return count;
        }

        public void TextboxValidation(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !((Char.IsDigit(e.Text, 0) || ((e.Text == System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString()) && (DS_Count(((TextBox)sender).Text) < 1))));
        }


        public ReaultUC()
        {
            InitializeComponent();
            //   foreach (TextBox t in Grid.Children.OfType<TextBox>())
            {
                //         t.PreviewTextInput += TextboxValidation;
            }

            // Deserialize();

        }




        public void Serialize()
        {
            FieldInfo[] fields = Formules.GetFieldInfo();

            Dictionary<string, object> Data = new Dictionary<string, object>();

            foreach (FieldInfo Info in fields)
            {
                Data.Add(Info.Name, Info.GetValue(FormSample));
            }

            JsonSerializer formatter = new JsonSerializer();
            using (StreamWriter fs = new StreamWriter("InputData.txt"))
            {
                formatter.Serialize(fs, Data);
            }
        }



        /// <summary>
        /// ЗАПИСЬ РАСЧЁТНЫХ ЗНАЧЕНИЙ В ТЕКСТБОКСЫ
        /// </summary>

        public void GetAssigningValues()
        {
            // Промежуточные значения

            TB_v_gaz.Text = Math.Round(FilterFormules.Formules.v_gaz, 3).ToString(); // Расход очищаемого газа в рабочих условиях
            TB_F_PA.Text = Math.Round(FilterFormules.Formules.F_PA, 3).ToString();   // Площадь осаждения

            // Значения, относящиеся к электрическому фильтру

            TB_markFilter.Text = FormSample.markFilter; // Марка электрофильтра
            TB_NCK.Text = FormSample.NCK.ToString(); // Число секций
            TB_NG.Text = FormSample.NG.ToString();  // Число газовых проходов
            TB_H.Text = FormSample.H.ToString();   // Активная высота электродов
            TB_NP.Text = FormSample.NP.ToString();   // Число полей
            TB_L.Text = FormSample.L.ToString();   // Активная длина поля
            TB_FA.Text = FormSample.FA.ToString();   // Площадь активного сечения
            TB_HK.Text = FormSample.HK.ToString();    // Шаг между одноимёнными электродами
            TB_FOC.Text = FormSample.FOC.ToString(); // Общая площадь осаждения
            TB_LE.Text = FormSample.LE.ToString(); // Длина фильтра
            TB_HE.Text = FormSample.HE.ToString(); // Высота фильтра
            TB_BE.Text = FormSample.BE.ToString(); // Ширина фильтра

            TB_R.Text = FormSample.R.ToString(); // Эффективный радиус коронирующего электрода
            TB_WGE.Text = Math.Round(FormSample.WGE, 3).ToString(); // Уточнёная скорость газа в электрофильтре (!)
            TB_COR_EL.Text = FormSample.COR_EL.ToString(); // Тип коронирующего электрода

            // Параметры электрического фильтра

            TB_HP.Text = Math.Round(FormSample.HP, 3).ToString(); // Расстояние между коронирующим и осадительным электродами
            TB_FF.Text = Math.Round(FormSample.FF, 3).ToString(); // Удельная осадительная поверхность
            TB_TT.Text = Math.Round(FormSample.TT, 3).ToString(); // Время пребывания газа в электрофильтре
            TB_EKR.Text = Math.Round(FormSample.EKR, 3).ToString();  // Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности
            TB_UKR.Text = Math.Round(FormSample.UKR, 3).ToString();  // Критическое напряжение электрического поля у коронирующего электрода
            TB_EOC.Text = Math.Round(FormSample.EOC, 3).ToString();  // Напряженность электрического поля вблизи поверхности осаждения пыли

            TB_W_W_1.Text = Math.Round(FormSample.W_W[0], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            TB_W_W_2.Text = Math.Round(FormSample.W_W[1], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            TB_W_W_3.Text = Math.Round(FormSample.W_W[2], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            TB_W_W_4.Text = Math.Round(FormSample.W_W[3], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            TB_W_W_5.Text = Math.Round(FormSample.W_W[4], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            TB_W_W_6.Text = Math.Round(FormSample.W_W[5], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций

            TB_Z_Z_1.Text = Math.Round(FormSample.Z_Z[0], 10).ToString(); // Фракционная степень очистки газа
            TB_Z_Z_2.Text = Math.Round(FormSample.Z_Z[1], 10).ToString(); // Фракционная степень очистки газа
            TB_Z_Z_3.Text = Math.Round(FormSample.Z_Z[2], 10).ToString(); // Фракционная степень очистки газа
            TB_Z_Z_4.Text = Math.Round(FormSample.Z_Z[3], 10).ToString(); // Фракционная степень очистки газа
            TB_Z_Z_5.Text = Math.Round(FormSample.Z_Z[4], 10).ToString(); // Фракционная степень очистки газа
            TB_Z_Z_6.Text = Math.Round(FormSample.Z_Z[5], 10).ToString(); // Фракционная степень очистки газа


            TB_st_och_gaza.Text = Math.Round(FormSample.st_och_gaza, 3).ToString(); // Общая аналитическая степень очистки газа
            TB_WE.Text = Math.Round(FormSample.WE, 3).ToString(); // Физическая скорость дрейфа частиц
            TB_KOP.Text = Math.Round(FormSample.KOP, 3).ToString(); // Количество осаждаемой пыли
            TB_KUP.Text = Math.Round(FormSample.KUP, 3).ToString(); // Количество уносимой из фильтра пыли
            TB_pylemk.Text = Math.Round(FormSample.pylemk, 3).ToString(); // Рациональная пылеемкость осадительных электродов
            TB_tvstr.Text = Math.Round(FormSample.tvstr, 3).ToString(); // Рекомендуемое время между очередным встряхиванием осадительных электродов
            TB_I.Text = Math.Round(FilterFormules.Formules.I, 3).ToString(); // Сила тока, необходимая на питание одного поля электрофильтра         

            // Значения, относящиеся к агрегату питания

            TB_PMark.Text = FormSample.PMark; // Марка агрегата питания
            TB_UMAX.Text = FormSample.UMAX.ToString(); // Максимальное выпрямленное напряжение
            TB_UU.Text = FormSample.UU.ToString(); // Среднее выпрямленное напряжение
            TB_IL.Text = FormSample.IL.ToString(); // Выпрямленный ток
            TB_IP.Text = FormSample.IP.ToString(); // Потребляемый ток                                         
            TB_UC.Text = FormSample.UC.ToString(); // Напряжение сети
            TB_NC.Text = FormSample.NC.ToString(); // Потребляемая из сети мощность
            TB_KPD.Text = FormSample.KPD.ToString(); // КПД агрегата питания
            TB_kf_a.Text = FormSample.kf_a.ToString(); // Коэффициент мощности

            // Параметры агрегата питания

            TB_a_ag.Text = Math.Round(FormSample.a_ag, 3).ToString(); // Расчетная потребляемая мощность агрегата на очистку газа
            TB_a_obsh.Text = Math.Round(FormSample.a_obsh, 3).ToString(); // Общая мощность, потребляемая агрегатом питания
            TB_n_ag.Text = FormSample.a_ag.ToString();                // Количество агрегатов питания на электрофильтр

        }

        /// <summary>
        /// МЕТОД НАЖАТИЯ НА КНОПКУ "РАСЧЁТ"
        /// </summary>
        private void ReportInit_Click(object sender, RoutedEventArgs e)
        {
            ReportInitializate();
        }

<<<<<<< HEAD


        private void MenuItem_Click(object sender, RoutedEventArgs e)
=======
        private void Report_Click(object sender, RoutedEventArgs e)
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538
        {
            Report.Content = "Создание отчета...";
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = "Отчёт.xlsx";
            saveFileDialog.Filter = "Excel (.xlsx ,.xls)|*.xlsx;*.xls";
            saveFileDialog.RestoreDirectory = true;

<<<<<<< HEAD
        public static List<AgregatPitaniya> ATF = new List<AgregatPitaniya>()
        {
            new AgregatPitaniya
=======
            if (saveFileDialog.ShowDialog() != true)
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538
            {
                return;
            }
<<<<<<< HEAD
        };
        public static List<IGAFilter> IGA = new List<IGAFilter>()
        {
            new IGAFilter
            {
                Id = 1,
                f_mark = "ЭГА1-10-6-4-2-330-5",
                f_active_lenght = 2.56,
                f_square_active =16.5,
                f_square_total = 634,
                f_overall_dim = new int[] {9260, 4890, 12400},
                NCK = 1,
                NG = 10,
                H = 6,
                N_osad = 4,
                N_el_pole = 2,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 2,
                f_mark = "ЭГА1-10-6-4-3-330-5",
                f_active_lenght = 2.56,
                f_square_active =16.5,
                f_square_total = 952,
                f_overall_dim = new int[] {13440, 4890, 12400},
                NCK = 1,
                NG = 10,
                H = 6,
                N_osad = 4,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 3,
                f_mark = "ЭГА1-10-6-6-2-330-5",
                f_active_lenght = 3.84,
                f_square_active =16.5,
                f_square_total = 952,
                f_overall_dim = new int[] {11820, 48920, 13400},
                NCK = 1,
                NG = 10,
                H = 6,
                N_osad = 6,
                N_el_pole = 2,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 4,
                f_mark = "ЭГА1-10-6-6-3-330-5",
                f_active_lenght = 3.84,
                f_square_active =16.5,
                f_square_total = 1428,
                f_overall_dim = new int[] {17280, 48920, 13400},
                NCK = 1,
                NG = 10,
                H = 6,
                N_osad = 6,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 5,
                f_mark = "ЭГА1-14-7.5-4-3-330-5",
                f_active_lenght = 2.56,
                f_square_active =28.7,
                f_square_total = 1656,
                f_overall_dim = new int[] {13440, 6120, 13900},
                NCK = 1,
                NG = 14,
                H = 7.5,
                N_osad = 4,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 6,
                f_mark = "ЭГА1-14-7.5-4-4-330-5",
                f_active_lenght = 2.56,
                f_square_active =28.7,
                f_square_total = 2208,
                f_overall_dim = new int[] {17620, 6120, 13900},
                NCK = 1,
                NG = 14,
                H = 7.5,
                N_osad = 4,
                N_el_pole = 4,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 7,
                f_mark = "ЭГА1-14-7.5-6-2-330-5",
                f_active_lenght = 3.84,
                f_square_active =28.7,
                f_square_total = 1656,
                f_overall_dim = new int[] {11820, 6190, 11900},
                NCK = 1,
                NG = 14,
                H = 7.5,
                N_osad = 6,
                N_el_pole =2,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 8,
                f_mark = "ЭГА1-14-7.5-6-3-330-5",
                f_active_lenght = 3.84,
                f_square_active =28.7,
                f_square_total = 2484,
                f_overall_dim = new int[] {17280, 6190, 14900},
                NCK = 1,
                NG = 14,
                H = 7.5,
                N_osad = 6,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 9,
                f_mark = "ЭГА1-20-7.5-4-3-330-5",
                f_active_lenght = 2.56,
                f_square_active =41,
                f_square_total = 2366,
                f_overall_dim = new int[] {13440, 7920, 15400},
                NCK = 1,
                NG = 20,
                H = 7.5,
                N_osad = 4,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 10,
                f_mark = "ЭГА1-20-7.5-4-4-330-5",
                f_active_lenght = 2.56,
                f_square_active =41,
                f_square_total = 3152,
                f_overall_dim = new int[] {17620, 7920, 15400},
                NCK = 1,
                NG = 20,
                H = 7.5,
                N_osad = 4,
                N_el_pole =4,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 11,
                f_mark = "ЭГА1-20-7.5-6-2-330-5",
                f_active_lenght = 3.84,
                f_square_active =41,
                f_square_total = 2366,
                f_overall_dim = new int[] {11820, 7990, 15400},
                NCK = 1,
                NG = 20,
                H = 7.5,
                N_osad = 6,
                N_el_pole = 2,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 12,
                f_mark = "ЭГА1-20-7.5-6-3-330-5",
                f_active_lenght = 3.83,
                f_square_active =41,
                f_square_total = 3549,
                f_overall_dim = new int[] {17280, 7990, 15400},
                NCK = 1,
                NG = 20,
                H = 7.5,
                N_osad = 6,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 13,
                f_mark = "ЭГА1-20-9-6-2-330-5",
                f_active_lenght = 3.84,
                f_square_active =49,
                f_square_total = 2826,
                f_overall_dim = new int[] {11820, 7990, 16900},
                NCK = 1,
                NG = 20,
                H = 9,
                N_osad = 6,
                N_el_pole = 2,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id =14,
                f_mark = "ЭГА1-20-9-6-3-330-5",
                f_active_lenght = 3.84,
                f_square_active =49,
                f_square_total = 4239,
                f_overall_dim = new int[] {17280,7990, 16900},
                NCK = 1,
                NG = 20,
                H = 9,
                N_osad = 6,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 15,
                f_mark = "ЭГА1-20-9-6-4-330-5",
                f_active_lenght = 3.84,
                f_square_active =49,
                f_square_total = 5652,
                f_overall_dim = new int[] {22740, 7990, 16900},
                NCK = 1,
                NG = 20,
                H = 9,
                N_osad = 6,
                N_el_pole = 4,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 16,
                f_mark = "ЭГА1-30-7.5-4-3-330-5",
                f_active_lenght = 2.56,
                f_square_active =61.4,
                f_square_total = 3549,
                f_overall_dim = new int[] {13440, 10950, 14900},
                NCK = 1,
                NG = 30,
                H = 7.5,
                N_osad = 4,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 17,
                f_mark = "ЭГА1-30-7.5-4-4-330-5",
                f_active_lenght = 2.56,
                f_square_active =61.4,
                f_square_total = 4732,
                f_overall_dim = new int[] {17620, 10950, 14900},
                NCK = 1,
                NG = 30,
                H = 7.5,
                N_osad = 4,
                N_el_pole = 4,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 18,
                f_mark = "ЭГА1-30-7.5-6-2-330-5",
                f_active_lenght = 3.84,
                f_square_active =61.4,
                f_square_total = 3549,
                f_overall_dim = new int[] {11820, 10950, 14900},
                NCK = 1,
                NG = 30,
                H = 7.5,
                N_osad = 6,
                N_el_pole = 2,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 19,
                f_mark = "ЭГА1-30-7.5-6-3-330-5",
                f_active_lenght = 3.84,
                f_square_active =61.4,
                f_square_total = 5322,
                f_overall_dim = new int[] {17280, 10990, 14900},
                NCK = 1,
                NG = 30,
                H = 7.5,
                N_osad = 6,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 20,
                f_mark = "ЭГА1-30-9-6-2-330-5",
                f_active_lenght = 3.84,
                f_square_active =73.4,
                f_square_total = 4240,
                f_overall_dim = new int[] {11820, 10990, 16400},
                NCK = 1,
                NG = 30,
                H = 9,
                N_osad = 6,
                N_el_pole = 2,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 21,
                f_mark = "ЭГА1-30-9-6-3-330-5",
                f_active_lenght = 3.84,
                f_square_active = 73.4,
                f_square_total = 6360,
                f_overall_dim = new int[] {17280, 10990, 16400},
                NCK = 1,
                NG = 30,
                H = 9,
                N_osad = 6,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 22,
                f_mark = "ЭГА1-30-9-6-4-330-5",
                f_active_lenght = 3.83,
                f_square_active =73.4,
                f_square_total = 8480,
                f_overall_dim = new int[] {22740, 20990, 26400},
                NCK = 1,
                NG = 30,
                H = 9,
                N_osad = 6,
                N_el_pole = 4,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 23,
                f_mark = "ЭГА1-30-12-6-3-330-5",
                f_active_lenght = 3.84,
                f_square_active =97.4,
                f_square_total = 8433,
                f_overall_dim = new int[] {17280, 10990, 19400},
                NCK = 1,
                NG = 30,
                H = 12,
                N_osad = 6,
                N_el_pole = 3,
                t_rab = 330,
                P_rab = 5,

            },
            new IGAFilter
            {
                Id = 24,
                f_mark = "ЭГА1-30-12-6-4-330-5",
                f_active_lenght = 3.84,
                f_square_active =97.4,
                f_square_total = 11244,
                f_overall_dim = new int[] {22740, 10990, 19400},
                NCK = 1,
                NG = 30,
                H = 12,
                N_osad = 6,
                N_el_pole = 4,
                t_rab = 330,
                P_rab = 5,
=======
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538

            string fileName = saveFileDialog.FileName;

            Excel.Workbook workBook = null;
            try
            {
                //Объявляем приложение
                Excel.Application app = new Excel.Application();
                //Добавить рабочую книгу
                workBook = app.Workbooks.Add(Type.Missing);
                //Получаем первый лист документа (счет начинается с 1)
                Excel.Worksheet sheet = (Excel.Worksheet)app.Worksheets.get_Item(1);
                //Заполнение ячеек
                sheet.Range["A1"].Value2 = "Расход очищаемого газа в рабочих условиях";
                sheet.Range["B1"].Value2 = TB_v_gaz.Text;
                sheet.Range["A2"].Value2 = "Площадь осаждения";
                sheet.Range["B2"].Value2 = TB_F_PA.Text;


<<<<<<< HEAD
            },

        };
        public static List<IGTFilter> IGT = new List<IGTFilter>()
        {
            new IGTFilter
            {
                Id = 1,
                f_mark = "ЭГТ2-3-2,5-20",
                f_square_total = 1448,
                f_overall_dim = new double[] {12.6, 4.93, 17.86},
                mod_filtr = 2,
                N_polei_long = 3,
                L = 2.5,
                FA = 20,

            },
            new IGTFilter
            {
                Id = 2,
                f_mark = "ЭГТ2-4-2,5-20",
                f_square_total = 1930,
                f_overall_dim = new double[] {16.6, 4.93, 17.86},
                mod_filtr = 2,
                N_polei_long = 4,
                L = 2.5,
                FA = 20,
            },
            new IGTFilter
            {
                Id = 3,
                f_mark = "ЭГТ2-3-2,5-30",
                f_square_total = 2238,
                f_overall_dim = new double[] {12.6, 6.23, 17.16},
                mod_filtr = 2,
                N_polei_long = 3,
                L = 2.5,
                FA = 30,
            },
            new IGTFilter
            {
                Id = 4,
                f_mark = "ЭГТ2-4-2,5-30",
                f_square_total = 2984,
                f_overall_dim = new double[] {16.6, 6.23, 17.16},
                mod_filtr = 2,
                N_polei_long = 4,
                L = 2.5,
                FA = 30,
            },
            new IGTFilter
            {
                Id = 5,
                f_mark = "ЭГГ2-3-2,5-40",
                f_square_total = 2895,
                f_overall_dim = new double[] {12.6, 7.79, 17.86},
                mod_filtr = 2,
                N_polei_long = 3,
                L = 2.5,
                FA = 40,
            },
            new IGTFilter
            {
                Id = 6,
                f_mark = "ЭГТ2-4-2,5-40",
                f_square_total = 3860,
                f_overall_dim = new double[] {16.6, 7.79, 17.86},
                mod_filtr = 2,
                N_polei_long = 4,
                L = 2.5,
                FA = 40,
            },
            new IGTFilter
            {
                Id = 7,
                f_mark = "ЭГТ2-3-2,5-60",
                f_square_total = 5790,
                f_overall_dim = new double[] {16.6, 10.64, 17.16},
                mod_filtr = 2,
                N_polei_long = 3,
                L = 2.5,
                FA = 40,
            }
        };
        public static List<UVFIiltercs> UV = new List<UVFIiltercs>()
        {
            new UVFIiltercs
            {
                Id = 1,
                f_mark = "УВ-2Х10",
                f_square_active = 21,
                f_square_total = 1200,
                f_active_lenght = 7.4,
                f_overall_dim = new double[] {6, 4.5,19.9},
                NCK = 2,
                H = 10,
            },
            new UVFIiltercs
            {
                Id = 2,
                f_mark = "УВ-3Х10",
                f_square_active = 32,
                f_square_total = 1800,
                f_active_lenght = 7.4,
                f_overall_dim = new double[] {9, 4.5,19.9},
                NCK = 3,
                H = 10,
            },
            new UVFIiltercs
            {
                Id = 3,
                f_mark = "УВ-1Х16",
                f_square_active = 16,
                f_square_total = 900,
                f_active_lenght = 7.4,
                f_overall_dim = new double[] {4.5, 4.5,19.9},
                NCK = 1,
                H = 16,
            },
            new UVFIiltercs
            {
                Id = 4,
                f_mark = "УВ-2Х16",
                f_square_active = 32,
                f_square_total = 1800,
                f_active_lenght = 7.4,
                f_overall_dim = new double[] {9, 4.5,19.9},
                NCK = 2,
                H = 16,
            },
            new UVFIiltercs
            {
                Id = 5,
                f_mark = "УВ-2Х24",
                f_square_active = 48,
                f_square_total = 2600,
                f_active_lenght = 7.4,
                f_overall_dim = new double[] {9, 6,21.4},
                NCK = 2,
                H = 24,
            },
            new UVFIiltercs
            {
                Id = 6,
                f_mark = "УВ-3Х24",
                f_square_active = 72,
                f_square_total = 3900,
                f_active_lenght = 7.4,
                f_overall_dim = new double[] {13.5, 6,21.4},
                NCK = 3,
                H = 24,
            },
        };
        public void AgrigateChoise()
        {
            var agrigat = ATF.FirstOrDefault(x => x.Id == 1);
            var markagrigat = agrigat.p_average_rectified_current;
            if (Formules.I >= markagrigat) ;


        }

        

        private void Report_Click(object sender, RoutedEventArgs e)
        {
            Report.Content = "Создание отчета...";
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = "Отчёт.xlsx";
            saveFileDialog.Filter = "Excel (.xlsx ,.xls)|*.xlsx;*.xls";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() != true)
            {
                return;
            }

            string fileName = saveFileDialog.FileName;

            Excel.Workbook workBook = null;
            try
            {
                //Объявляем приложение
                Excel.Application app = new Excel.Application();
                //Добавить рабочую книгу
                workBook = app.Workbooks.Add(Type.Missing);
                //Получаем первый лист документа (счет начинается с 1)
                Excel.Worksheet sheet = (Excel.Worksheet)app.Worksheets.get_Item(1);
                //Заполнение ячеек
                sheet.Range["A1"].Value2 = "Расход очищаемого газа в рабочих условиях";
                sheet.Range["B1"].Value2 = TB_v_gaz.Text;
                sheet.Range["A2"].Value2 = "Площадь осаждения";
                sheet.Range["B2"].Value2 = TB_F_PA.Text;

                Excel.Range range = sheet.Range["A1", "B2"];
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                sheet.Range["A4"].Value2 = "Марка электрофильтра";
                sheet.Range["B4"].Value2 = TB_markFilter.Text;
                sheet.Range["A5"].Value2 = "Число секций";
                sheet.Range["B5"].Value2 = TB_NCK.Text;
                sheet.Range["A6"].Value2 = "Число газовых проходов";
                sheet.Range["B6"].Value2 = TB_NG.Text;
                sheet.Range["A7"].Value2 = "Активная высота электродов";
                sheet.Range["B7"].Value2 = TB_H.Text;
                sheet.Range["A8"].Value2 = "Число полей";
                sheet.Range["B8"].Value2 = TB_NP.Text;
                sheet.Range["A9"].Value2 = "Активная длина поля";
                sheet.Range["B9"].Value2 = TB_L.Text;
                sheet.Range["A10"].Value2 = "Площадь активного сечения";
                sheet.Range["B10"].Value2 = TB_FA.Text;
                sheet.Range["A11"].Value2 = "Шаг между одноимёнными электродами";
                sheet.Range["B11"].Value2 = TB_HK.Text;
                sheet.Range["A12"].Value2 = "Общая площадь осаждения";
                sheet.Range["B12"].Value2 = TB_NP.Text;
                sheet.Range["A13"].Value2 = "Активная длина поля";
                sheet.Range["B13"].Value2 = TB_LE.Text;
                sheet.Range["A14"].Value2 = "Длина фильтра";
                sheet.Range["B14"].Value2 = TB_FA.Text;
                sheet.Range["A15"].Value2 = "Высота фильтра";
                sheet.Range["B15"].Value2 = TB_HE.Text;
                sheet.Range["A15"].Value2 = "Ширина фильтра";
                sheet.Range["B15"].Value2 = TB_BE.Text;

                range = sheet.Range["A4", "B15"];
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                sheet.Range["A17"].Value2 = "Эффективный радиус коронирующего электрода";
                sheet.Range["B17"].Value2 = TB_R.Text;
                sheet.Range["A18"].Value2 = "Уточнёная скорость газа в электрофильтре";
                sheet.Range["B18"].Value2 = TB_WGE.Text;
                sheet.Range["A19"].Value2 = "Тип коронирующего электрода";
                sheet.Range["B19"].Value2 = TB_COR_EL.Text;

                range = sheet.Range["A17", "B19"];
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                sheet.Range["A21"].Value2 = "Расстояние между коронирующим и осадительным электродами";
                sheet.Range["B21"].Value2 = TB_HP.Text;
                sheet.Range["A22"].Value2 = "Удельная осадительная поверхность";
                sheet.Range["B22"].Value2 = TB_FF.Text;
                sheet.Range["A23"].Value2 = "Время пребывания газа в электрофильтре";
                sheet.Range["B23"].Value2 = TB_TT.Text;
                sheet.Range["A24"].Value2 = "Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности";
                sheet.Range["B24"].Value2 = TB_EKR.Text;
                sheet.Range["A25"].Value2 = "Критическое напряжение электрического поля у коронирующего электрода";
                sheet.Range["B25"].Value2 = TB_UKR.Text;
                sheet.Range["A26"].Value2 = "Напряженность электрического поля вблизи поверхности осаждения пыли";
                sheet.Range["B26"].Value2 = TB_EOC.Text;

                range = sheet.Range["A21", "B26"];
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                sheet.Range["B28"].Value2 = TB_W_W_1.Text;
                sheet.Range["B29"].Value2 = TB_W_W_2.Text;
                sheet.Range["B30"].Value2 = TB_W_W_3.Text;
                sheet.Range["B31"].Value2 = TB_W_W_4.Text;
                sheet.Range["B32"].Value2 = TB_W_W_5.Text;
                sheet.Range["B33"].Value2 = TB_W_W_6.Text;
                sheet.Range["A28", "A33"].Merge();
                sheet.Range["A28", "A33"].Value2 = "Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций";

                range = sheet.Range["A28", "B33"];
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                sheet.Range["B35"].Value2 = TB_Z_Z_1.Text;
                sheet.Range["B36"].Value2 = TB_Z_Z_2.Text;
                sheet.Range["B37"].Value2 = TB_Z_Z_3.Text;
                sheet.Range["B38"].Value2 = TB_Z_Z_4.Text;
                sheet.Range["B39"].Value2 = TB_Z_Z_5.Text;
                sheet.Range["B40"].Value2 = TB_Z_Z_6.Text;
                sheet.Range["A35", "A40"].Merge();
                sheet.Range["A35", "A40"].Value2 = "Фракционная степень очистки газа";

                range = sheet.Range["A35", "B40"];
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                sheet.Range["A42"].Value2 = "Общая аналитическая степень очистки газа";
                sheet.Range["B42"].Value2 = TB_st_och_gaza.Text;
                sheet.Range["A43"].Value2 = "Физическая скорость дрейфа частиц";
                sheet.Range["B43"].Value2 = TB_WE.Text;
                sheet.Range["A44"].Value2 = "Количество осаждаемой пыли";
                sheet.Range["B44"].Value2 = TB_KOP.Text;
                sheet.Range["A45"].Value2 = "Количество уносимой из фильтра пыли";
                sheet.Range["B45"].Value2 = TB_KUP.Text;
                sheet.Range["A46"].Value2 = "Рациональная пылеемкость осадительных электродов";
                sheet.Range["B46"].Value2 = TB_pylemk.Text;
                sheet.Range["A47"].Value2 = "Рекомендуемое время между очередным встряхиванием осадительных электродов";
                sheet.Range["B47"].Value2 = TB_tvstr.Text;
                sheet.Range["A48"].Value2 = "Сила тока, необходимая на питание одного поля электрофильтра";
                sheet.Range["B48"].Value2 = TB_I.Text;

                range = sheet.Range["A42", "B48"];
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                sheet.Range["A50"].Value2 = "Марка агрегата питания";
                sheet.Range["B50"].Value2 = TB_PMark.Text;
                sheet.Range["A51"].Value2 = "Максимальное выпрямленное напряжение";
                sheet.Range["B51"].Value2 = TB_UMAX.Text;
                sheet.Range["A52"].Value2 = "Среднее выпрямленное напряжение";
                sheet.Range["B52"].Value2 = TB_KOP.Text;
                sheet.Range["A53"].Value2 = "Выпрямленный ток";
                sheet.Range["B53"].Value2 = TB_IL.Text;
                sheet.Range["A54"].Value2 = "Потребляемый ток ";
                sheet.Range["B54"].Value2 = TB_UC.Text;
                sheet.Range["A55"].Value2 = "Напряжение сети";
                sheet.Range["B55"].Value2 = TB_NC.Text;
                sheet.Range["A56"].Value2 = "Потребляемая из сети мощность";
                sheet.Range["B56"].Value2 = TB_I.Text;
                sheet.Range["A57"].Value2 = "КПД агрегата питания";
                sheet.Range["B57"].Value2 = TB_KPD.Text;
                sheet.Range["A58"].Value2 = "Коэффициент мощности";
                sheet.Range["B58"].Value2 = TB_kf_a.Text;

                range = sheet.Range["A50", "B58"];
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                sheet.Range["A60"].Value2 = "Расчетная потребляемая мощность агрегата на очистку газа";
                sheet.Range["B60"].Value2 = TB_a_ag.Text;
                sheet.Range["A61"].Value2 = "Общая мощность, потребляемая агрегатом питания";
                sheet.Range["B61"].Value2 = TB_a_obsh.Text;
                sheet.Range["A62"].Value2 = "Количество агрегатов питания на электрофильтр";
                sheet.Range["B62"].Value2 = TB_n_ag.Text;

                range = sheet.Range["A60", "B62"];
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
                range.Borders.Item[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                range = sheet.Range["A1", "B61"];
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                range.Font.Size = 12;
                range.EntireColumn.AutoFit();
                range.EntireRow.AutoFit();

                workBook.SaveAs(fileName);
                workBook.Application.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                workBook?.Close();
                workBook = null;
            }
=======
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                workBook?.Close();
                workBook = null;
            }
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538
            Report.Content = "Отчет";
        }
    }
}
