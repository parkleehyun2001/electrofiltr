﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElectricFilter.WpfApp
{
    public class UVFIiltercs
    {
        public int Id { get; set; }
        public string f_mark { get; set; }
        public double f_active_lenght { get; set; }
        public double f_square_active { get; set; }
        public double f_square_total { get; set; }
        public double LE { get; set; }
        public double HE { get; set; }
        public double BE { get; set; }
        public double NCK { get; set; }
        public double H { get; set; }
    }
}
