﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FastReport;
using FastReport.Export.Image;
using FastReport.Export.PdfSimple;


namespace ElectricFilter.WpfApp
{
    /// <summary>
    /// Логика взаимодействия для WindowReport.xaml
    /// </summary>
    public partial class WindowReport : Window
    {
        private string inFolder = @"..\..\..\in\";
        public WindowReport()
        {
            InitializeComponent();
            inFolder = Utils.FindDirectory("in");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateReport();

            // create report instance
            Report report = new Report();

            // load the existing report
            report.Load($@"{inFolder}\report.frx");

            // register the array
            //report.RegisterData(Categories, "Categories");
            ///report.RegisterData(Products, "Products");

            // prepare the report
            report.Prepare();

            // save prepared report
            //if (!Directory.Exists(outFolder))
            //    Directory.CreateDirectory(outFolder);
            report.SavePrepared("Prepared_Report.fpx");

            // export to image
            ImageExport image = new ImageExport();
            image.ImageFormat = ImageExportFormat.Jpeg;
            report.Export(image, "report.jpg");

            #region -- Export to PDF

            PDFSimpleExport pdfExport = new PDFSimpleExport();

            pdfExport.Export(report, "report.pdf");

            #endregion


            report.Dispose();

            //buttonPdf.IsEnabled = true;
            //buttonPicture.IsEnabled = true;

            //imagePreview.Source = "report.jpg";
        }

        private void CreateReport()
        {

        }
    }
}
