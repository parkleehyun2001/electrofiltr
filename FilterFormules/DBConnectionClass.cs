﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection
{

    public class ClassLibrary

    {

        // Подключение к базе данных Microsoft Access
        public string connectString = "Driver= {Filters.mdb}; Dbq = C:\\Users\\мечта\\Desktop\\Electrofiltr";

        public void OpenConnection()
        {
            // Инициализация подключения к базе данных Microsoft Excel
            var connection = new OdbcConnection(connectString);
            connection.Open();
        }
        public string markFilter;   // Марка электрофильтра
        public double NCK;          // Число секций
        public double NG;           // Число газовых проходов 
        public double H;            // Активная высота электродов
        public double L;            // Активная длина поля 
        public double NP;           // Число полей
        public double FA;           // Площадь активного сечения
        public double HK;           // Шаг между электродами
        public double FOC;          // Общая площадь осаждения
        public double LE;           // Длина электрофильтра
        public double HE;           // Высота электрофильтра
        public double BE;           // Ширина электрофильтра
        public double R;            // Эффективный радиус коронирующего электрода
        public double WGE;          // Уточнёная скорость газа в электрофильтре
        public string COR_EL;       // Тип коронирующего электрода
        public string PMark;        // Марка агрегата питания
        public double UMAX;         // Максимальное выпрямленное напряжение
        public double UU;           // Среднее выпрямленное напряжение
        public double IL;           // Выпрямленный ток
        public double IP;           // Потребляемый ток
        public double UC;           // Напряжение сети
        public double NC;           // Потребляемая из сети мощность
        public double KPD;          // КПД агрегата питания
        public double kf_a;         // Коэффициент мощности
        public double HP;           // Расстояние между коронирующим и осадительным электродами



        // ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ПО УМОЛЧАНИЮ ДЛЯ ФИЛЬТРОВ МАРКИ "ЭГА"
        public void GetDefaultNotationOfIGA()
        {
            // ПРИМЕЧАНИЯ К ЭЛЕКТРОФИЛЬТРАМ МАРКИ "ЭГА"
            HK = 0.3;  // Шаг между одноимёнными электродами
            R = 2 * (Math.Pow(10, -3)); // Эффективный радиус коронирующего электрода, м
            COR_EL = "игольчатый"; // Тип коронирующего электрода
            HP = 0.133; // Расстояние между коронирующим и осадительным электродами
        }


        // ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ПО УМОЛЧАНИЮ ДЛЯ ФИЛЬТРОВ МАРКИ "ЭГТ"

        public void GetDefaultNotationOfIGT()
        {
            // ПРИМЕЧАНИЯ К ЭЛЕКТРОФИЛЬТРАМ МАРКИ "ЭГТ"
            NG = 1; // Число газовых проходов
            NP = 1; // Число полей
            NCK = 1; // Число секций
            H = 7.5; // Активная высота электродов 
            HK = 0.26; // Шаг между электродами
            R = 0.0011; // Эффективный радиус коронирующего электрода
            COR_EL = "гладкий"; // Тип коронирующего электрода
            HP = 0.1; // Расстояние между коронирующим и осадительным электродами
        }


        // ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ПО УМОЛЧАНИЮ ДЛЯ ФИЛЬТРОВ МАРКИ "УВ"

        public void GetDefaultNotationOfUV()
        {
            // ПРИМЕЧАНИЯ К ЭЛЕКТРОФИЛЬТРАМ МАРКИ "УВ"
            NG = 1; // Число газовых проходов
            NP = 1; // Число полей
            HK = 0.275; // Шаг между одноимёнными электродами
            R = 0.002; // Эффективный радиус коронирующего электрода
            COR_EL = "игольчатый"; // Тип коронирующего электрода
            HP = 0.131; // Расстояние между коронирующим и осадительным электродами
        }

        // ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ИЗ НАЗВАНИЯ ЭЛЕКТРОФИЛЬТРА МАРКИ "ЭГА"

        public void GetDecryptionsOfNameFilterIGA(string filterName)
        {
            string[] filter = filterName.Split(new char[] { '-' });

            // List числовых значений, полученных из названия фильтра
            List<double> decryptionNumbers = new List<double>();

            bool isNameOfFilter = true;
            // Добавление значения в List
            foreach (string s in filter)
            {

                if (isNameOfFilter)
                {
                    markFilter = s[0].ToString() + s[1].ToString() + s[2].ToString(); // Марка электрофильтра

                    string numSection = s[3].ToString();
                    double num = Convert.ToDouble(numSection);

                    decryptionNumbers.Add(num);
                }
                else
                {
                    double filterDecryption = Convert.ToDouble(s);
                    decryptionNumbers.Add(filterDecryption);
                }

                isNameOfFilter = false;
            }

            NCK = Convert.ToDouble(decryptionNumbers[0]); // Число секций
            NG = Convert.ToDouble(decryptionNumbers[1]); // Число газовых проходов
            H = Convert.ToDouble(decryptionNumbers[2]); // Активная высота электродов
            NP = Convert.ToDouble(decryptionNumbers[4]); // Число полей

        }
        // ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ИЗ НАЗВАНИЯ ЭЛЕКТРОФИЛЬТРА МАРКИ "ЭГТ"

        public void GetDecryptionsOfNameFilterIGT(string filterName)
        {
            string[] filter = filterName.Split(new char[] { '-' });

            // List числовых значений, полученных из названия фильтра
            List<double> decryptionNumbers = new List<double>();

            bool isNameOfFilter = true;
            // Добавление значения в List
            foreach (string s in filter)
            {
                if (isNameOfFilter)
                {
                    markFilter = s[0].ToString() + s[1].ToString() + s[2].ToString(); // Марка электрофильтра
                }
                else
                {
                    double filterDecryption = Convert.ToDouble(s);

                    decryptionNumbers.Add(filterDecryption);
                }

                isNameOfFilter = false;
            }

            NP = Convert.ToDouble(decryptionNumbers[0]); // Число полей
            L = Convert.ToDouble(decryptionNumbers[1]); // Активная длина поля
            FA = Convert.ToDouble(decryptionNumbers[2]); // Площадь активного сечения

        }

        // ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ИЗ НАЗВАНИЯ ЭЛЕКТРОФИЛЬТРА МАРКИ "УВ"

        public void GetDecryptionsOfNameFilterUV(string filterName)
        {
            string[] filter = filterName.Split(new char[] { '-' });

            // List числовых значений, полученных из названия фильтра
            List<double> decryptionNumbers = new List<double>();

            bool isNameOfFilter = true;
            // Добавление значения в List
            foreach (string s in filter)
            {
                if (isNameOfFilter == true)
                {
                    markFilter = s.ToString(); // Марка электрофильтра
                }
                else if (isNameOfFilter == false)
                {
                    string numSection = s[0].ToString();
                    string activeHeight = s[2].ToString() + s[3].ToString();
                    double numS = Convert.ToDouble(numSection);
                    double activeH = Convert.ToDouble(activeHeight);

                    decryptionNumbers.Add(numS);
                    decryptionNumbers.Add(activeH);
                }

                isNameOfFilter = false;
            }

            NCK = Convert.ToDouble(decryptionNumbers[0]); // Число секций
            H = Convert.ToDouble(decryptionNumbers[1]); // Активная высота электродов

        }
        // ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ФИЛЬТРА МАРКИ "ЭГА"

        public void SelectFilterOfIGA(double F_PA)
        {
            string query = "SELECT f_id, f_mark, f_active_length, f_square_active, f_square_total, f_overall_dim FROM IGA ORDER BY f_id";


            OdbcCommand command = new OdbcCommand(query);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                FA = Convert.ToDouble(reader[3]);  // Площадь активного сечения

                if (FA >= F_PA && F_PA >= 16.5) // 16.5 - минимальная площадь для фильтров "ЭГА"
                {
                    GetDefaultNotationOfIGA();

                    string filterName = reader[1].ToString();
                    GetDecryptionsOfNameFilterIGA(filterName);

                    L = Convert.ToDouble(reader[2]);  // Активная длина поля
                    FOC = Convert.ToDouble(reader[4]); // Общая площадь осаждения

                    string[] filter = reader[5].ToString().Split(new char[] { 'х' });

                    LE = Convert.ToDouble(filter[0]) * Math.Pow(10, -3); // Длина фильтра, м

                    // В данном случае округление реализовано специально для тестов (это не повлияет на конечный результат),
                    // поскольку при передаче значения в object происходит "баг".
                    // К примеру, значение "19.4" (19400) будет выглядеть как "19.0000000002"
                    HE = Math.Round(Convert.ToDouble(filter[2]) * Math.Pow(10, -3), 1); // Высота фильтра, м
                    BE = Convert.ToDouble(filter[1]) * Math.Pow(10, -3); // Ширина фильтра, м

                    break;
                }
                else
                {
                    FA = 0;
                    HP = 0;
                }
            }

            reader.Close();
        }
        // ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ФИЛЬТРА МАРКИ "ЭГТ"

        public void SelectFilterOfIGT(double FOC)
        {
            string query = "SELECT f_id, f_mark, f_square_total, f_overall_dim FROM IGT ORDER BY f_id";

            OdbcCommand command = new OdbcCommand(query);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                GetDecryptionsOfNameFilterIGT(reader[1].ToString());

                if (FA >= FOC && FOC >= 20) // 20 - минимальная площадь для фильтров "ЭГТ"
                {
                    GetDefaultNotationOfIGT(); // Получаем значения из примечаний под таблицей (см. методичку)

                    FOC = Convert.ToDouble(reader[2]); // Общая площадь осаждения

                    string[] filter = reader[3].ToString().Split(new char[] { 'x' });

                    LE = Convert.ToDouble(filter[0]); // Длина фильтра, м
                    HE = Convert.ToDouble(filter[2]); // Высота фильтра, м
                    BE = Convert.ToDouble(filter[1]); // Ширина фильтра, м

                    break;
                }
                else
                {
                    markFilter = "";
                    NP = 0;
                    LE = 0;
                    FA = 0;
                    HP = 0;
                }
            }

            reader.Close();

        }

        // ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ФИЛЬТРА МАРКИ "УВ"

        public void SelectFilterOfUV(double F_PA)
        {
            string query = "SELECT f_id, f_mark, f_square_active, f_square_total,  f_active_length, f_overall_dim FROM UV ORDER BY f_id";

            OdbcCommand command = new OdbcCommand(query);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                FA = Convert.ToDouble(reader[2]);  // Площадь активного сечения

                if (FA >= FOC && FOC >= 21) // 21 - минимальная площадь фильтров "УВ"
                {
                    GetDefaultNotationOfUV(); // Получаем значения из примечаний под таблицей (см. методичку)

                    string filterName = reader[1].ToString();
                    GetDecryptionsOfNameFilterUV(filterName);

                    L = Convert.ToDouble(reader[4]);  // Активная длина поля                    
                    FOC = Convert.ToDouble(reader[3]); // Общая площадь осаждения

                    string[] filter = reader[5].ToString().Split(new char[] { 'х' });

                    LE = Convert.ToDouble(filter[0]); // Длина фильтра, м
                    HE = Convert.ToDouble(filter[2]); // Высота фильтра, м
                    BE = Convert.ToDouble(filter[1]); // Ширина фильтра, м

                    break;
                }
                else
                {
                    FA = 0;
                    HP = 0;
                }

            }

            reader.Close();
        }

        // ВЫБОР ФИЛЬТРА

        public void GetSelectedFilter(double tr, double F_PA)
        {
            OpenConnection();

            // Максимально допустимые температуры газа для всех типов фильтров
            double T_MAX_IGA = 350;
            double T_MAX_IGT = 425;
            double T_MAX_UV = 250;

            if (tr <= T_MAX_UV)
            {
                SelectFilterOfUV(FOC);

            }
            else if (tr <= T_MAX_IGA)
            {
                SelectFilterOfIGA(FOC);

            }
            else if (tr <= T_MAX_IGT)
            {
                SelectFilterOfIGT(FOC);
            }
        }

        // ВЫБОР АГРЕГАТА ПИТАНИЯ

        public void SelectAgPit(double I)
        {
            string query = "SELECT p_id, p_mark, p_rectified_voltage, p_average_rectified_current, p_current, p_voltage, p_power, p_efficiency_factor, p_power_coefficient FROM ATF ORDER BY p_id";


            OpenConnection();

            OdbcCommand command = new OdbcCommand(query);
            var reader = command.ExecuteReader();


            while (reader.Read())
            {
                if (Convert.ToDouble(reader[3]) >= I)
                {
                    PMark = reader[1].ToString();

                    string rectifiedVoltage = reader[2].ToString();
                    string[] rectifiedVoltageNumbers = rectifiedVoltage.Split(new char[] { '/' });

                    UMAX = Convert.ToDouble(rectifiedVoltageNumbers[0]) * Math.Pow(10, 3);
                    UU = Convert.ToDouble(rectifiedVoltageNumbers[1]) * Math.Pow(10, 3);

                    IL = Convert.ToDouble(reader[3].ToString());
                    IP = Convert.ToDouble(reader[4].ToString());

                    string voltage = reader[5].ToString();
                    string[] voltageNumbers = voltage.Split(new char[] { ',' });

                    UC = Convert.ToDouble(voltageNumbers[0]);

                    NC = Convert.ToDouble(reader[6].ToString());
                    KPD = Convert.ToDouble(reader[7].ToString());
                    kf_a = Convert.ToDouble(reader[8].ToString());
                    break;
                }
            }

            reader.Close();
        }


    }
}


