﻿using System;
using System.Collections.Generic;
using System.Reflection;
<<<<<<< HEAD
using Lists;

=======
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lists;
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538

namespace FilterFormules
{
    public class Formules
    {
       

        public Formules(double _vro, double _x, double _tr, double _pbar, double _pgi, double _w, List<double> _spyli, double _kpyli, double _vz_gaza, double _sr_tok, double _ud_sopr)
        {
            vro = _vro; // Расход сухого газа на очистку при нормальных условиях
            x = _x; // Cодержание водяных паров в газе
            tr = _tr; // Температура очищаемого газа на входе
            pbar = _pbar; // Барометрическое давление на местности
            pgi = _pgi; // Избыточное давление очищаемого газа
            w = _w; // Рекомендуемая скорость очищаемого газа
            spyli = _spyli; // Фракционный состав пыли
            kpyli = _kpyli; // Концентрация пыли на входе
            sr_tok = _sr_tok; // Заданный средний ток короны
            ud_sopr = _ud_sopr; // Удельное электрическое сопротивление слоя пыли
            vz_gaza = _vz_gaza;  // Динамическая вязкость газа при рабочих условиях
        }

        
        public double vro;
        public double x;
        public double tr;
        public double pbar;
        public double pgi;
        public double w;
        public double kpyli;
        public double sr_tok;
        public double ud_sopr;
        public double vz_gaza;

        public List<double> spyli = new List<double>();    // Фракционный состав пыли

        // ПОСТОЯННЫЕ ЗНАЧЕНИЯ, НЕОБХОДИМЫЕ ДЛЯ РЕШЕНИЯ

        public const double KT = 1.4;   // Коэффициент формы кривой тока [1.1 - 1.4]
        public const double KE = 1.412; // Коэффициент перехода от амплитудного значения напряжения к эффективному [1.405 - 1.412]

        // РАСЧЕТ ПРОМЕЖУТОЧНЫХ ЗНАЧЕНИЙ

        public static double v_gaz;  // Расход очищаемого газа в рабочих условиях 
        public static double F_PA;  // Площадь активного сечения электрофильтра

        //ВЫБОР ТИПА ЭЛЕКТРОФИЛЬТРА

        public string markFilter;   // Марка электрофильтра
        public double NCK;          // Число секций
        public double NG;           // Число газовых проходов 
        public double H;            // Активная высота электродов
        public double L;            // Активная длина поля 
        public double NP;           // Число полей
        public double FA;           // Площадь активного сечения
        public double HK;           // Шаг между электродами
        public double FOC;          // Общая площадь осаждения
        public double LE;           // Длина электрофильтра
        public double HE;           // Высота электрофильтра
        public double BE;           // Ширина электрофильтра
        public double R;            // Эффективный радиус коронирующего электрода
        public double WGE;          // Уточнёная скорость газа в электрофильтре
        public string COR_EL;       // Тип коронирующего электрода


        // РАСЧЕТ ПАРАМЕТРОВ ЭЛЕКТРОФИЛЬТРА

        public double HP;           // Расстояние между коронирующим и осадительным электродами
        public double FF;           // Удельная осадительная поверхность
        public double TT;           // Время пребывания газа в электрофильтре
        public double EKR;          // Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности
        public double UKR;          // Критическое напряжение электрического поля у коронирующего электрода
        public double EOC;          // Напряженность электрического поля вблизи поверхности осаждения пыли
        public double popravka;     // Переменная для упрощения (поправка)
        public List<double> W_W = new List<double>();        // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
        public List<double> Z_Z = new List<double>();        //Фракционная степень очистки газа 

        public double A1;           // Коэффициент дрейфа
        public double A2;           // Коэффициент дрейфа

        public double st_och_gaza;  // Общая аналитическая степень очистки газа
        public double WE;           // Физическая скорость дрейфа частиц
        public double KOP;          // Количество осаждаемой пыли
        public double KUP;          // Количество уносимой из фильтра пыли
        public double pylemk;       // Рациональная пылеёмкость осадительных электродов
        public double tvstr;        // Рекомендуемое время между очередным встряхиванием
        public static double I;            // Сила тока, необходимая на питания одного поля электрофильтра

        // ВЫБОР АГРЕГАТА ПИТАНИЯ

        public string PMark;        // Марка агрегата питания
        public double UMAX;         // Максимальное выпрямленное напряжение
        public double UU;           // Среднее выпрямленное напряжение
        public double IL;           // Выпрямленный ток
        public double IP;           // Потребляемый ток
        public double UC;           // Напряжение сети
        public double NC;           // Потребляемая из сети мощность
        public double KPD;          // КПД агрегата питания
        public double kf_a;         // Коэффициент мощности
        public double a_ag;         // Расчётная потребляемая мощность агрегата на очистку газа
        public double a_obsh;       // Общая мощность, потребляемая агрегатом питания
        public double n_ag;         // Количество агрегатов питания на электрофильтр

        // ПРИМЕЧАНИЕ

        public double a_nom;        // Номинальная мощность 
        public double potoku;       // Коэффициент загрузки агрегата по току
        public double poU;          // Коэффициент загрузки агрегата по напряжению
        public double formKr;       // Коэффициент формы кривой тока
        public double perex;        // Коэффициент перехода от амплитудного значения напряжения к эффективному

        // Экземпляр класса математической библиотеки
        ApproxMethods AppMet = new ApproxMethods();

        // РАСЧЁТ ВСЕХ ЗНАЧЕНИЙ
        public void GetAllValues()
        {
             // Очистка некоторых значений электрофильтра и агрегата питания
            ReloadValues();

            // Очистка коллекций для возможности повторных расчётов 
            W_W.Clear();
            Z_Z.Clear();

            // РАСЧЁТ ПРОМЕЖУТОЧНЫХ ЗНАЧЕНИЙ

            v_gaz = ApproxMethods.GetWasteAmount(vro, x, tr, pbar, pgi);   // Расход очищаемого газа в рабочих условиях
            F_PA = ApproxMethods.GetlocalSpace(v_gaz, w);                  // Расчётная площадь осаждения

            // ВЫБОР ТИПА ЭЛЕКТРОФИЛЬТРА

            bool found = false;

<<<<<<< HEAD
            foreach (IGAFilter filter in ListsClass.IGA)
=======
            foreach (IGAFilter filter in ListClass.IGA)
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538
            {
                if (F_PA <= filter.f_square_active)
                {
                    markFilter = filter.f_mark;
                    NCK = filter.NCK;
                    NG = filter.NG;
                    H = filter.H;
                    L = filter.f_active_lenght;
                    FA = filter.f_square_active;
                    FOC = filter.f_square_total;
                    LE = filter.LE;
                    BE = filter.BE;
                    HE = filter.HE;

                    NP = 1;
                    HK = 0.275;
                    R = 0.002;
                    COR_EL = "игольчатый";
                    HP = 0.131;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
<<<<<<< HEAD
                foreach (IGTFilter filter in ListsClass.IGT)
=======
                foreach (IGTFilter filter in ListClass.IGT)
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538
                {
                    if (F_PA <= filter.f_square_total)
                    {
                        markFilter = filter.f_mark;
                        L = filter.L;
                        FA = filter.FA;
                        FOC = filter.f_square_total;
                        LE = filter.LE;
                        BE = filter.BE;
                        HE = filter.HE;


                        NG = 1; // Число газовых проходов
                        NP = 1; // Число полей
                        NCK = 1; // Число секций
<<<<<<< HEAD
                        H = 7.5; // Активная высота электродов
=======
                        H = 7.5; // Активная высота электродов 
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538
                        HK = 0.26; // Шаг между электродами
                        R = 0.0011; // Эффективный радиус коронирующего электрода
                        COR_EL = "гладкий"; // Тип коронирующего электрода
                        HP = 0.1; // Расстояние между коронирующим и осадительным электродами

                        found = true;
                        break;
                    }
                }
            }

            if (!found)
            {
<<<<<<< HEAD
                foreach (UVFIiltercs filter in ListsClass.UV)
=======
                foreach (UVFIiltercs filter in ListClass.UV)
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538
                {
                    if (F_PA <= filter.f_square_active)
                    {
                        markFilter = filter.f_mark;
                        NCK = filter.NCK;
                        H = filter.H;
                        L = filter.f_active_lenght;
                        FOC = filter.f_square_total;
                        LE = filter.LE;
                        BE = filter.BE;
                        HE = filter.HE;

                        NG = 1; // Число газовых проходов
                        NP = 1; // Число полей
                        HK = 0.275; // Шаг между одноимёнными электродами
                        R = 0.002; // Эффективный радиус коронирующего электрода
                        COR_EL = "игольчатый"; // Тип коронирующего электрода
                        HP = 0.131; // Расстояние между коронирующим и осадительным электродами

                        found = true;
                        break;
                    }
                }
            }
<<<<<<< HEAD

=======
>>>>>>> ebad80a8970099cf29c8c4a5c6d463b9dc3c4538

            // РАСЧЁТ ПАРАМЕТРОВ ЭЛЕКТРОФИЛЬТРА

            WGE = ApproxMethods.GetUpdatedSpeed(v_gaz, FA);                              // Уточнёная скорость газа для электрофильтра
            FF = ApproxMethods.GetHurryPlane(NP, L, HP, WGE);                            // Удельная осадительная поверхность
            TT = ApproxMethods.GetGasResidenceTime(L, WGE);                              // Время пребывания газа в электрофильтре
            EKR = ApproxMethods.GetCriticalFieldOfNegativeElectrode(tr, pbar, pgi, R);   // Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности
            UKR = ApproxMethods.GetCriticaVoltageOfElectrode(EKR, R, HK, HP);            // Критическое напряжение электрического поля у коронирующего электрода
            EOC = ApproxMethods.GetElectricFieldOflocalSpace(sr_tok);                    // Напряженность электрического поля вблизи поверхности осаждения пыли

            for (int i = 0; i < 6; i++)
            {
                W_W.Add(ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli[i], sr_tok, WGE, vz_gaza, COR_EL)); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций [6 значений]
            }

            for (int i = 0; i < 6; i++)
            {
                Z_Z.Add(ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W[i], L, WGE, HP)); // Фракционная степень очистки газа [6 значений]
            }

            st_och_gaza = ApproxMethods.GetGeneralAnalyticalNotchOfCleaning(Z_Z);  // Общая аналитическая степень очистки газа
            WE = ApproxMethods.GetPhysicalSpeedOfParticles(W_W);                   // Физическая скорость дрейфа частиц 
            KOP = ApproxMethods.GetAmountHurryOfDust(st_och_gaza, kpyli, v_gaz);    // Количество осаждаемой пыли
            KUP = ApproxMethods.GetAmountRemovedOfDust(st_och_gaza, kpyli, v_gaz); // Количество уносимой из фильтра пыли
            pylemk = ApproxMethods.GetRationalDustOfElectrodes(ud_sopr);           // Рациональная пылеемкость осадительных электродов
            tvstr = ApproxMethods.GetRecTimeBtwShakeOfElectr(FOC, pylemk, v_gaz, kpyli, st_och_gaza); //Рекомендуемое время между очередным встряхиванием осадительных электродов

            // ВЫБОР АГРЕГАТА ПИТАНИЯ

            I = ApproxMethods.GetReqCurToPowOneFieldOfFilter(sr_tok, H, L, NG, HK);  // Сила тока, необходимая на питание одного поля электрофильтра

            foreach (AgregatPitaniya agreg in ListsClass.ATF)
            {
                if (I <= agreg.p_average_rectified_current)
                {
                    PMark = agreg.p_mark;
                    UMAX = agreg.p_rectifiedMax;
                    UU = agreg.p_rectifiedMin;
                    IL = agreg.p_average_rectified_current;
                    IP = agreg.p_current;
                    NC = agreg.p_power;
                    KPD = agreg.p_efficency;
                    UC = agreg.p_voltage;
                    kf_a = agreg.p_power_co;
                    break;
                }
            }

            // РАСЧЁТ ПАРАМЕТРОВ АГРЕГАТА ПИТАНИЯ

            a_ag = ApproxMethods.GetApproxPowerOfConsumption(I, IL, UU, UMAX);              // Расчетная потребляемая мощность агрегата на очистку газа, кВа
            a_obsh = ApproxMethods.GetTotalPowOfConsumpt(UMAX, IL, KT, KE, kf_a, KPD, a_ag); // Общая мощность, потребляемая агрегатом питания
            n_ag = ApproxMethods.GetNumbOfAgg(a_ag);                                        // Количество агрегатов питания на электрофильтр
        }


        public void ReloadValues()
        {
            HK = 0;   // Шаг между одноимёнными электродами
            R = 0;    // Эффективный радиус коронирующего электрода, м

            markFilter = ""; // Марка электрофильтра
            NCK = 0;         // Число секций
            NG = 0;          // Число газовых проходов
            H = 0;           // Активная высота электродов
            NP = 0;          // Число полей

            L = 0;   // Активная длина поля
            FA = 0;  // Площадь активного сечения
            FOC = 0; // Общая площадь осаждения

            LE = 0; // Длина электрофильтра
            HE = 0; // Высота электрофильтра
            BE = 0; // Ширина электрофильтра


            PMark = ""; // Марка питания агрегата
            UMAX = 0;   // Выпрямленное напряжение (максимальное)
            UU = 0;     // Выпрямленное напряжение (среднее)
            IL = 0;     // Выпрямленный ток 
            IP = 0;     // Потребляемый ток 
            UC = 0;     // Напряжение сети 
            NC = 0;     // Потребляемая из сети мощность
            KPD = 0;    // КПД агрегата питания
            kf_a = 0;   // Коэффициент мощности
        }


        public static FieldInfo[] GetFieldInfo()
        {
            FieldInfo[] fields = typeof(Formules).GetFields();

            return fields;
        }
    }
    public class ApproxMethods
    {

        // Полезные константы
        public const double PI = Math.PI;


        // РАСХОД ОЧИЩАЕМОГО ГАЗА В РАБОЧИХ УСЛОВИЯХ (v_gaz)

        /// <param name="vro">Расход сухого газа на очистку при нормальных условиях</param>
        /// <param name="x">Содержание водяных паров в газе</param>
        /// <param name="tr">Температура очищаемого газа на входе</param>
        /// <param name="pbar">Барометрическое давление на местности</param>
        /// <param name="pgi">Избыточное давление очищаемого газа</param>

        public static double GetWasteAmount(double vro, double x, double tr, double pbar, double pgi)
        {
            return vro * (1 + (x * (Math.Pow(10, -3)) / 0.804)) * (tr + 273) * ((101.3) / (273 * (pbar + pgi)));
        }


        // ПЛОЩАДЬ АКТИВНОГО СЕЧЕНИЯ ЭЛЕКТРОФИЛЬТРА (F_PA)

        /// <param name="v_gaz">Расход очищаемого газа в рабочих условиях</param>
        /// <param name="w">Рекомендуемая скорость очищаемого газа</param>

        public static double GetlocalSpace(double v_gaz, double w)
        {
            return v_gaz / (3600 * w);
        }

        // УТОЧНЁНАЯ СКОРОСТЬ ГАЗА ДЛЯ ЭЛЕКТРОФИЛЬТРА (WGE)

        /// <param name="v_gaz">Расход очищаемого газа в рабочих условиях</param>
        /// <param name="FA">Площадь активного сечения электрофильтра</param>
        /// <returns></returns>
        public static double GetUpdatedSpeed(double v_gaz, double FA)
        {
            return v_gaz / (3600 * FA);
        }


        // УДЕЛЬНАЯ ОСАДИТЕЛЬНАЯ ПОВЕРХНОСТЬ (FF)

        /// <param name="NP">Число полей в электрофильтре</param>
        /// <param name="L">Активная длина поля электрофильтра</param>
        /// <param name="HP">Расстояние между коронирующим и осадительным электродами в электрофильтре</param>
        /// <param name="WGE">Уточнёная скорость газа для электрофильтра</param>
        /// <returns></returns>
        public static double GetHurryPlane(double NP, double L, double HP, double WGE)
        {
            return (NP * L) / (HP * WGE);
        }

        // ВРЕМЯ ПРЕБЫВАНИЯ ГАЗА В ЭЛЕКТРОФИЛЬТРЕ (TT)

        /// <param name="L">Активная длина поля электрофильтра</param>
        /// <param name="WGE">Уточнёная скорость газа для электрофильтра</param>
        /// <returns></returns>
        public static double GetGasResidenceTime(double L, double WGE)
        {
            return L / WGE;
        }


        // КРИТИЧЕСКАЯ НАПРЯЖЕННОСТЬ ЭЛЕКТРИЧЕСКОГО ПОЛЯ ДЛЯ КОРОНИРУЮЩЕГО ЭЛЕКТРОДА ОТРИЦАТЕЛЬНОЙ ПОЛЯРНОСТИ (EKR)

        /// <param name="tr">Температура очищаемого газа на входе</param>
        /// <param name="pbar">Барометрическое давление на местности</param>
        /// <param name="pgi">Избыточное давление очищаемого газа</param>
        /// <param name="R">Эффективный радиус коронирующего электрода в электрофильтре</param>
        /// <returns></returns>
        public static double GetCriticalFieldOfNegativeElectrode(double tr, double pbar, double pgi, double R)
        {
            double B = 273 / (101.3 * (tr + 273)) * (pbar + pgi); // Переменная для упрощения (повторяющееся значение)

            return 3.039 * (Math.Pow(10, 6)) * (B + 0.0311 * Math.Sqrt(B / R));
        }

        // КРИТИЧЕСКОЕ НАПРЯЖЕНИЕ ЭЛЕКТРИЧЕСКОГО ПОЛЯ У КОРОНИРУЮЩЕГО ЭЛЕКТРОДА (UKR)

        /// <param name="EKR">Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности</param>
        /// <param name="R">Эффективный радиус коронирующего электрода в электрофильтре</param>
        /// <param name="HK">Шаг между одноимёнными электродами в электрофильтре</param>
        /// <param name="HP">Расстояние между коронирующим и осадительными электродами</param>
        /// <returns></returns>
        public static double GetCriticaVoltageOfElectrode(double EKR, double R, double HK, double HP)
        {
            double D = 0; // Вспомогательная переменная
            double condition = HP / HK; // Вспомогательная переменная

            if (condition >= 1)
            {
                D = (HK / 2) * Math.Exp((PI * HP) / HK);

            }
            else if (condition < 1)
            {

                D = (4 * HP) / PI;
            }

            return EKR * R * Math.Log(D / R);
        }


        // НАПРЯЖЕННОСТЬ ЭЛЕКТРИЧЕСКОГО ПОЛЯ ВБЛИЗИ ПОВЕРХНОСТИ ОСАЖДЕНИЯ ПЫЛИ (EOC)

        /// <param name="sr_tok">Заданный средний ток короны для электрофильтра</param>
        /// <returns></returns>
        public static double GetElectricFieldOflocalSpace(double sr_tok)
        {
            return 5.48 * (Math.Pow(10, 5)) * Math.Sqrt(sr_tok);
        }

        // ФИЗИЧЕСКАЯ СКОРОСТЬ ДРЕЙФА ЧАСТИЦ ПО АНАЛИТИЧЕСКИМ ДАННЫМ ДЛЯ ЗАДАННЫХ ФРАКЦИЙ (W_W)

        /// <param name="EOC">Напряженность электрического поля вблизи поверхности осаждения пыли</param>
        /// <param name="spyli">Фракционный состав пыли</param>
        /// <param name="sr_tok">Заданный средний ток короны для электрофильтра</param>
        /// <param name="WGE">Уточнёная скорость газа для электрофильтра</param>
        /// <param name="vz_gaza">Динамическая вязкость газа при рабочих условиях</param>
        /// <param name="COR_EL">Тип коронирующего электрода в электрофильтре</param>
        /// <returns></returns>
        public static double GetPhysicalAnalyticalOfFractions(double EOC, double spyli, double sr_tok, double WGE, double vz_gaza, string COR_EL)
        {
            // Коэффициенты дрейфа
            double a_1;
            double a_2;

            // Условие на тип коронирующего электрода
            if (COR_EL == "игольчатый")
            {
                a_1 = 3;
                a_2 = 0.2;

            }
            else
            {
                a_1 = 9;
                a_2 = 0.2;
            }
            return 8.16 * EOC * Math.Sqrt(spyli * (a_1 * Math.Sqrt(sr_tok * Math.Pow(10, -3)) * a_2 * WGE) * Math.Pow(10, -6) * ((1 / (4 * PI * 9 * Math.Pow(10, 9))) / (vz_gaza)));
        }


        // ФРАКЦИОННАЯ СТЕПЕНЬ ОЧИСТКИ ГАЗА (Z_Z)

        /// <param name="NP">Число полей в электрофильтре</param>
        /// <param name="W_W">Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций</param>
        /// <param name="L">Активная длина поля электрофильтра</param>
        /// <param name="WGE">Уточнёная скорость газа в электрофильтре</param>
        /// <param name="HP">Расстояние между коронирующим и осадительным электродами</param>
        /// <returns></returns>
        public static double GetFractionalNotchOfCleaning(double NP, double W_W, double L, double WGE, double HP)
        {
            return 1 - Math.Exp(-NP * W_W * L / (WGE * HP));
        }

        // ОБЩАЯ АНАЛИТИЧЕСКАЯ СТЕПЕНЬ ОЧИСТКИ ГАЗА (st_och_gaza)

        /// <param name="Z_Z">Фракционная степень очистки газа</param>

        public static double GetGeneralAnalyticalNotchOfCleaning(List<double> Z_Z)
        {
            // Вспомогательная переменная
            double summ = 0;

            // Принято решение расчёта среднего значения:
            for (int i = 0; i < Z_Z.Count; i++)
            {
                summ += Z_Z[i];
            }

            return summ / Z_Z.Count;
        }


        // ФИЗИЧЕСКАЯ СКОРОСТЬ ДРЕЙФА ЧАСТИЦ (WE)

        /// <param name="W_W">Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций</param>
        /// <returns></returns>
        public static double GetPhysicalSpeedOfParticles(List<double> W_W)
        {
            // Вспомогательная переменная
            double summ = 0;

            for (int i = 0; i < W_W.Count; i++)
            {
                summ += W_W[i];
            }

            return summ / W_W.Count;
        }

        // КОЛИЧЕСТВО ОСАЖДАЕМОЙ ПЫЛИ В ЭЛЕКТРОФИЛЬТРЕ (KOP)

        /// <param name="st_och_gaza">Общая аналитическая степень очистки газа</param>
        /// <param name="kpyli">Концентрация пыли на входе</param>
        /// <param name="v_gaz">Расход очищаемого газа в рабочих условиях</param>
        /// <returns></returns>
        public static double GetAmountHurryOfDust(double st_och_gaza, double kpyli, double v_gaz)
        {
            return Math.Pow(10, -3) * st_och_gaza * kpyli * v_gaz;
        }


        // КОЛИЧЕСТВО УНОСИМОЙ ИЗ ФИЛЬТРА ПЫЛИ (KUP)

        /// <param name="st_och_gaza">Общая аналитическая степень очистки газа</param>
        /// <param name="kpyli">Концентрация пыли на входе</param>
        /// <param name="v_gaz">Расход очищаемого газа в рабочих условиях</param>
        /// <returns></returns>
        public static double GetAmountRemovedOfDust(double st_och_gaza, double kpyli, double v_gaz)
        {
            return Math.Pow(10, -3) * (1 - st_och_gaza) * kpyli * v_gaz;
        }

        // РАЦИОНАЛЬНАЯ ПЫЛЕЁМКОСТЬ ОСАДИТЕЛЬНЫХ ЭЛЕКТРОДОВ В ЭЛЕКТРОФИЛЬТРЕ (pylemk)

        /// <param name="ud_sopr">Удельное электрическое сопротивление слоя пыли</param>
        /// <returns></returns>

        public static double GetRationalDustOfElectrodes(double ud_sopr)
        {
            return 3.2 - 0.267 * Math.Log10(ud_sopr);
        }

        // РЕКОМЕНДУЕМОЕ ВРЕМЯ МЕЖДУ ОЧЕРЕДНЫМ ВСТРЯХИВАНИЕМ ОСАДИТЕЛЬНЫХ ЭЛЕКТРОДОВ В ЭЛЕКТРОФИЛЬТРЕ (tvstr)

        /// <param name="FOC">Общая площадь осаждения электрофильтра</param>
        /// <param name="pylemk">Рациональная пылеемкость осадительных электродов</param>
        /// <param name="v_gaz">Расход очищаемого газа в рабочих условиях</param>
        /// <param name="kpyli">Концентрация пыли на входе</param>
        /// <param name="st_och_gaza">Общая аналитическая степень очистки газа</param>
        /// <returns></returns>
        public static double GetRecTimeBtwShakeOfElectr(double FOC, double pylemk, double v_gaz, double kpyli, double st_och_gaza)
        {
            return (16.7 * FOC * pylemk) / (v_gaz * kpyli * st_och_gaza);
        }


        // СИЛА ТОКА, НЕОБХОДИМАЯ НА ПИТАНИЕ ОДНОГО ПОЛЯ ЭЛЕКТРОФИЛЬТРА (I)

        /// <param name="sr_tok">Заданный средний ток короны для электрофильтра</param>
        /// <param name="H">Активная высота электродов в электрофильтре</param>
        /// <param name="L">Активная длина поля в электрофильтре</param>
        /// <param name="NG">Число газовых проходов в электрофильтре</param>
        /// <param name="HK">Шаг между одноимёнными электродами в электрофильтре</param>
        /// <returns></returns>
        public static double GetReqCurToPowOneFieldOfFilter(double sr_tok, double H, double L, double NG, double HK)
        {
            return sr_tok * ((H * L * NG) / (HK));
        }

        // РАСЧЁТНАЯ ПОТРЕБЛЯЕМАЯ МОЩНОСТЬ АГРЕГАТА НА ОЧИСТКУ ГАЗА (a_ag)

        /// <param name="I">Сила тока, необходимая на питание одного поля электрофильтра</param>
        /// <param name="IL">Выпрямленный ток агрегата питания</param>
        /// <param name="UU">Среднее выпрямленное напряжение агрегата питания</param>
        /// <param name="UMAX">Максимальное выпрямленное напряжение агрегата питания</param>
        /// <returns></returns>
        public static double GetApproxPowerOfConsumption(double I, double IL, double UU, double UMAX)
        {
            // Вспомогательные переменные
            double a_nom = UU * IL * Math.Pow(10, -6); // Номинальная мощность
            double potoku = I / IL; // Коэффициент загрузки агрегата по току
            double poU = UMAX / UU; // Коэффициент загрузки агрегата по напряжению

            return potoku * poU * a_nom;
        }

        // ОБЩАЯ МОЩНОСТЬ, ПОТРЕБЛЯЕМАЯ АГРЕГАТОМ ПИТАНИЯ (a_obsh)

        /// <param name="UMAX">Максимальное выпрямленное напряжение агрегата питания</param>
        /// <param name="IL">Выпрямленный ток агрегата питания</param>
        /// <param name="KT">Коэффициент формы кривой тока [1.1 - 1.4]</param>
        /// <param name="KE">Коэффициент перехода от амплитудного значения напряжения к эффективному [1.405 - 1.412]</param>
        /// <param name="kf_a">Коэффициент мощности агрегата питания</param>
        /// <param name="KPD">КПД агрегата питания</param>
        /// <param name="a_ag">Расчётная потребляемая мощность агрегата на очистку газа</param>
        /// <returns></returns>
        public static double GetTotalPowOfConsumpt(double UMAX, double IL, double KT, double KE, double kf_a, double KPD, double a_ag)
        {
            double N_ALL = 0.05 * a_ag;
            return (UMAX * IL * KT * kf_a / (KE * KPD) + N_ALL) * Math.Pow(10, -6);
        }


        // КОЛИЧЕСТВО АГРЕГАТОВ ПИТАНИЯ НА ЭЛЕКТРОФИЛЬТР (n_ag)

        /// <param name="NP">Число полей в электрофильтре</param>
        /// <returns></returns>
        public static double GetNumbOfAgg(double NP)
        {
            return NP;
        }

        
    }

}


