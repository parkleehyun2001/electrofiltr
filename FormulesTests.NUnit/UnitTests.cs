using NUnit.Framework;
using FilterFormules;
using System.Collections.Generic;
using System;
using ElectricFilter;

namespace FormulesTests.NUnit
{
    public class Tests
    {
        ApproxMethods AppM = new ApproxMethods();

        static double v_ro = 108000;  // ������ ������ ���� �� ������� ��� ���������� ��������, �3/� 
        static double x = 100;     // ���������� ������� ����� � ����, �/�3
        static double tr = 300; // ����������� ���������� ���� �� �����, C
        static double pbar = 101.5; // ��������������� �������� �� ���������, ���
        static double pgi = 9.5;  // ���������� �������� ���������� ����, ���
        static double w = 0.8;     // ������������� �������� ���������� ����, �/c
        static double sr_tok = 0.22; // �������� ������� ��� ������
        static double kpyli = 30; // ������������ ���� �� �����
        static double vz_gaza = Math.Pow(2.2, -5); // ������������ �������� ���� ��� ������� ��������
        static double ud_sopr = 100000000; // �������� ������������� ������������� ���� ����
        static List<double> spyli = new List<double>() { 2, 10, 15, 25, 50, 75 }; // ����������� ������ ����

        Formules FormSample = new Formules(v_ro, x, tr, pbar, pgi, w, spyli, kpyli, sr_tok, vz_gaza,ud_sopr);

        // ������ ���������� ���� � ������� �������� (v_gaz)
        [Test]
        public void TestWasteAmount()
        {
            double vro = 108000;
            double x = 100;
            double tr = 300;
            double pbar = 101.5;
            double pgi = 9.5;
            double expected = 232602.60;

            var actual = ApproxMethods.GetWasteAmount(vro, x, tr, pbar, pgi);
            Assert.AreEqual(expected, actual, 0.01d);
        }


        // ������� ��������� ������� �������������� (F_PA)
        [Test]
        public void TestlocalSpace()
        {
            double v_gaz = 232602.60;
            double w = 0.8;
            double expected = 80.76;

            var actual = ApproxMethods.GetlocalSpace(v_gaz, w);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ����ͨ��� �������� ���� ��� �������������� (WGE)
        [Test]
        public void TestUpdatedSpeed()
        {
            double v_gaz = 232602.60;
            double FA = 97.4;
            double expected = 0.66;

            var actual = ApproxMethods.GetUpdatedSpeed(v_gaz, FA);
            Assert.AreEqual(expected, actual, 0.01d);
        }

         // �������� ������������ ����������� (FF)
        [Test]
        public void TestHurryPlane()
        {
            double NP = 3;
            double L = 3.84;
            double HP = 0.133;
            double WGE = 0.663365866;
            double expected = 130.57;

            var actual = ApproxMethods.GetHurryPlane(NP, L, HP, WGE);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ����� ���������� ���� � �������������� (TT)
        [Test]
        public void TestGasResidenceTime()
        {
            double L = 3.84;
            double WGE = 0.663365866;
            double expected = 5.78;

            var actual = ApproxMethods.GetGasResidenceTime(L, WGE);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ����������� ������������� �������������� ���� ��� ������������� ��������� ������������� ���������� (EKR)
        [Test]
        public void TestCriticalFieldOfNegativeElectrode()
        {
            double tr = 300;
            double pbar = 101.5;
            double pgi = 9.5;
            double R = 0.002;
            double expected = 3113536.88;

            var actual = ApproxMethods.GetCriticalFieldOfNegativeElectrode(tr, pbar, pgi, R);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ����������� ���������� �������������� ���� � ������������� ��������� (UKR)
        [Test]
        public void TestCriticaVoltageOfElectrode()
        {
            double EKR = 3113536.885;
            double R = 0.002;
            double HK = 0.3;
            double HP = 0.133;
            double expected = 27640.52;

            var actual = ApproxMethods.GetCriticaVoltageOfElectrode(EKR, R, HK, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ������������� �������������� ���� ������ ����������� ��������� ���� (EOC)
        [Test]
        public void TestElectricFieldOflocalSpace()
        {
            double sr_tok = 0.22;
            double expected = 257034.78;

            var actual = ApproxMethods.GetElectricFieldOflocalSpace(sr_tok);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ���������� �������� ������ ������ �� ������������� ������ ��� �������� ������� 1-6 (W_W)
        [Test]
        public void TestPhysicalAnalyticalOfFractions1()
        {
            double EOC = 257034.7836;
            double spyli = 2;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "����������";
            double expected = 0.0048;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions2()
        {
            double EOC = 257034.7836;
            double spyli = 10;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "����������";
            double expected = 0.0108;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions3()
        {
            double EOC = 257034.7836;
            double spyli = 15;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "����������";
            double expected = 0.0133;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions4()
        {
            double EOC = 257034.7836;
            double spyli = 25;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "����������";
            double expected = 0.0172;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions5()
        {
            double EOC = 257034.7836;
            double spyli = 50;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "����������";
            double expected = 0.0243;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions6()
        {
            double EOC = 257034.7836;
            double spyli = 75;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "����������";
            double expected = 0.0297;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ����������� ������� ������� ���� 1-6 (Z_Z)
        [Test]
        public void TestFractionalNotchOfCleaning1()
        {
            double NP = 3;
            double W_W = 0.004865029;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.47;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning2()
        {
            double NP = 3;
            double W_W = 0.010878535;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.75;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning3()
        {
            double NP = 3;
            double W_W = 0.01332343;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.82;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning4()
        {
            double NP = 3;
            double W_W = 0.017200475;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.89;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning5()
        {
            double NP = 3;
            double W_W = 0.024325145;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.95;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning6()
        {
            double NP = 3;
            double W_W = 0.029792096;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.97;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ����� ������������� ������� ������� ���� (st_och_gaza)
        [Test]
        public void TestGeneralAnalyticalNotchOfCleaning()
        {
            List<double> Z_Z = new List<double>() { 0.470188044, 0.758388562, 0.824419489, 0.894166512, 0.958254586, 0.979554503 };
            double expected = 0.814;

            var actual = ApproxMethods.GetGeneralAnalyticalNotchOfCleaning(Z_Z);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ���������� �������� ������ ������ (WE)
        [Test]
        public void TestPhysicalSpeedOfParticles()
        {
            List<double> W_W = new List<double>() { 0.004865029, 0.010878535, 0.01332343, 0.017200475, 0.024325145, 0.029792036 };
            double expected = 0.0167;

            var actual = ApproxMethods.GetPhysicalSpeedOfParticles(W_W);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ���������� ���������� ���� � �������������� (KOP)
        [Test]
        public void TestAmountHurryOfDust()
        {
            double st_och_gaza = 0.814161949;
            double kpyli = 30;
            double v_gaza = 232602.6074;
            double expected = 5681.28;

            var actual = ApproxMethods.GetAmountHurryOfDust(st_och_gaza, kpyli, v_gaza);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ���������� �������� �� ������� ���� (KUP)
        [Test]
        public void TestAmountRemovedOfDust()
        {
            double st_och_gaza = 0.814161949;
            double kpyli = 30;
            double v_gaza = 232602.6074;
            double expected = 1296.79;

            var actual = ApproxMethods.GetAmountRemovedOfDust(st_och_gaza, kpyli, v_gaza);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ������������ ���Ũ������ ������������ ���������� � �������������� (pylemk)
        [Test]
        public void TestRationalDustOfElectrodes()
        {
            double ud_sopr = 100000000;
            double expected = 1.064;

            var actual = ApproxMethods.GetRationalDustOfElectrodes(ud_sopr);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ������������� ����� ����� ��������� ������������� ������������ ���������� � �������������� (tvstr)
        [Test]
        public void TestRecTimeBtwShakeOfElectr()
        {
            double FOC = 8433;
            double pylemk = 1.064;
            double v_gaz = 232602.6074;
            double kpyli = 30;
            double st_och_gaza = 0.814161949;
            double expected = 0.0263;

            var actual = ApproxMethods.GetRecTimeBtwShakeOfElectr(FOC, pylemk, v_gaz, kpyli, st_och_gaza);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ���� ����, ����������� �� ������� ������ ���� �������������� (I)
        [Test]
        public void TestReqCurToPowOneFieldOfFilter()
        {
            double sr_tok = 0.22;
            double H = 12;
            double L = 3.84;
            double NG = 30;
            double HK = 0.3;
            double expected = 1013.76;

            var actual = ApproxMethods.GetReqCurToPowOneFieldOfFilter(sr_tok, H, L, NG, HK);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ���ר���� ������������ �������� �������� �� ������� ���� (a_ag)
        [Test]
        public void TestApproxPowerOfConsumption()
        {
            double I = 1013.76;
            double IL = 1600;
            double UU = 50000;
            double UMAX = 80000;
            double expected = 81.1008;

            var actual = ApproxMethods.GetApproxPowerOfConsumption(I, IL, UU, UMAX);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ����� ��������, ������������ ��������� ������� (a_obsh)
        [Test]
        public void TestTotalPowOfConsumpt()
        {
            double UMAX = 80000;
            double IL = 1600;
            double KT = 1.4;
            double KE = 1.412;
            double kf_a = 0.8;
            double KPD = 0.9;
            double a_ag = 81.1008;
            double expected = 112.8108;

            var actual = ApproxMethods.GetTotalPowOfConsumpt(UMAX, IL, KT, KE, kf_a, KPD, a_ag);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // ���������� ��������� ������� �� ������������� (n_ag)
        [Test]
        public void TestNumbOfAgg()
        {
            double NP = 3;
            double expected = 3;

            var actual = ApproxMethods.GetNumbOfAgg(NP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        // �������������� ���� �������� ���� ���ר���� ����������
        [Test]
        public void TestAllValues()
        {
            double vro = 108000;
            double x = 100;
            double tr = 300;
            double pbar = 101.5;
            double pgi = 9.5;
            double expectedGWA = 232602.60;
            var actualGWA = ApproxMethods.GetWasteAmount(vro, x, tr, pbar, pgi);
            Assert.AreEqual(expectedGWA, actualGWA, 0.01d);

            double v_gaz = 232602.60;
            double w = 0.8;
            double expectedGlS = 80.76;
            var actualGlS = ApproxMethods.GetlocalSpace(v_gaz, w);
            Assert.AreEqual(expectedGlS, actualGlS, 0.01d);

            
            double FA = 97.4;
            double expectedGUS = 0.66;
            var actualGUS = ApproxMethods.GetUpdatedSpeed(v_gaz, FA);
            Assert.AreEqual(expectedGUS, actualGUS, 0.01d);

            double NP = 3;
            double L = 3.84;
            double HP = 0.133;
            double WGE = 0.663365866;
            double expectedGHP = 130.57;
            var actualGHP = ApproxMethods.GetHurryPlane(NP, L, HP, WGE);
            Assert.AreEqual(expectedGHP, actualGHP, 0.01d);

            
            double expectedGGRT = 5.78;
            var actualGGRT = ApproxMethods.GetGasResidenceTime(L, WGE);
            Assert.AreEqual(expectedGGRT, actualGGRT, 0.01d);

           
            double R = 0.002;
            double expectedGCFONE = 3113536.88;
            var actualGCFONE = ApproxMethods.GetCriticalFieldOfNegativeElectrode(tr, pbar, pgi, R);
            Assert.AreEqual(expectedGCFONE, actualGCFONE, 0.01d);

            double EKR = 3113536.885;
            double HK = 0.3;
            double expectedGCVOE = 27640.52;
            var actualGCVOE = ApproxMethods.GetCriticaVoltageOfElectrode(EKR, R, HK, HP);
            Assert.AreEqual(expectedGCVOE, actualGCVOE, 0.01d);

            double sr_tok = 0.22;
            double expectedGEFOlS = 257034.78;
            var actualGEFOlS = ApproxMethods.GetElectricFieldOflocalSpace(sr_tok);
            Assert.AreEqual(expectedGEFOlS, actualGEFOlS, 0.01d);

            double EOC = 257034.7836;
            double spyli1 = 2;
            double vz_gaza = 0.019403791;
            string COR_EL = "����������";
            double expectedGPAOF1 = 0.0048;
            var actualGPAOF1 = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli1, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expectedGPAOF1, actualGPAOF1, 0.01d);

            
            double spyli2 = 10;
            double expectedGPAOF2 = 0.0108;
            var actualGPAOF2 = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli2, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expectedGPAOF2, actualGPAOF2, 0.01d);

            double spyli3 = 15;
            double expectedGPAOF3 = 0.0133;
            var actualGPAOF3 = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli3, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expectedGPAOF3, actualGPAOF3, 0.01d);

            double spyli4 = 25;
            double expectedGPAOF4 = 0.0172;
            var actualGPAOF4 = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli4, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expectedGPAOF4, actualGPAOF4, 0.01d);

            double spyli5 = 50;
            double expectedGPAOF5 = 0.0243;
            var actualGPAOF5 = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli5, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expectedGPAOF5, actualGPAOF5, 0.01d);

            double spyli6 = 75;
            double expectedGPAOF6 = 0.0297;
            var actualGPAOF6 = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli6, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expectedGPAOF6, actualGPAOF6, 0.01d);

            double W_W1 = 0.004865029;
            double expectedGFNOC1 = 0.47;
            var actualGFNOC1 = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W1, L, WGE, HP);
            Assert.AreEqual(expectedGFNOC1, actualGFNOC1, 0.01d);

            double W_W2 = 0.010878535;
            double expectedGFNOC2 = 0.758;
            var actualGFNOC2 = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W2, L, WGE, HP);
            Assert.AreEqual(expectedGFNOC2, actualGFNOC2, 0.01d);
            
            double W_W3 = 0.01332343;
            double expectedGFNOC3 = 0.824;
            var actualGFNOC3 = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W3, L, WGE, HP);
            Assert.AreEqual(expectedGFNOC3, actualGFNOC3, 0.01d);

            double W_W4 = 0.017200475;
            double expectedGFNOC4 = 0.894;
            var actualGFNOC4 = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W4, L, WGE, HP);
            Assert.AreEqual(expectedGFNOC4, actualGFNOC4, 0.01d);

            double W_W5 = 0.024325145;
            double expectedGFNOC5 = 0.9582;
            var actualGFNOC5 = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W5, L, WGE, HP);
            Assert.AreEqual(expectedGFNOC5, actualGFNOC5, 0.01d);

            double W_W6 = 0.029792096;
            double expectedGFNOC6 = 0.979554503;
            var actualGFNOC6 = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W6, L, WGE, HP);
            Assert.AreEqual(expectedGFNOC6, actualGFNOC6, 0.01d);

            List<double> Z_Z = new List<double>() { 0.470188044, 0.758388562, 0.824419489, 0.894166512, 0.958254586, 0.979554503 };
            double expectedGGANOC = 0.814;
            var actualGGANOC = ApproxMethods.GetGeneralAnalyticalNotchOfCleaning(Z_Z);
            Assert.AreEqual(expectedGGANOC, actualGGANOC, 0.01d);

            List<double> W_W = new List<double>() { 0.004865029, 0.010878535, 0.01332343, 0.017200475, 0.024325145, 0.029792036 };
            double expectedGPSOP = 0.0167;
            var actualGPSOP = ApproxMethods.GetPhysicalSpeedOfParticles(W_W);
            Assert.AreEqual(expectedGPSOP, actualGPSOP, 0.01d);

            double st_och_gaza = 0.814161949;
            double kpyli = 30;
            double v_gaza = 232602.6074;
            double expectedGAHOD = 5681.28;
            var actualGAHOD = ApproxMethods.GetAmountHurryOfDust(st_och_gaza, kpyli, v_gaza);
            Assert.AreEqual(expectedGAHOD, actualGAHOD, 0.01d);

            double expectedGAROD = 1296.79;
            var actualGAROD = ApproxMethods.GetAmountRemovedOfDust(st_och_gaza, kpyli, v_gaza);
            Assert.AreEqual(expectedGAROD, actualGAROD, 0.01d);

            double ud_sopr = 100000000;
            double expectedGRDOE = 1.064;
            var actualGRDOE = ApproxMethods.GetRationalDustOfElectrodes(ud_sopr);
            Assert.AreEqual(expectedGRDOE, actualGRDOE, 0.01d);

            double FOC = 8433;
            double pylemk = 1.064;
            double expectedGTBSOE = 0.0263;
            var actualGTBSOE = ApproxMethods.GetRecTimeBtwShakeOfElectr(FOC, pylemk, v_gaz, kpyli, st_och_gaza);
            Assert.AreEqual(expectedGTBSOE, actualGTBSOE, 0.01d);

            double H = 12;
            double NG = 30;
            double expectedGRCTPOFOF = 1013.76;
            var actualGRCTPOFOF = ApproxMethods.GetReqCurToPowOneFieldOfFilter(sr_tok, H, L, NG, HK);
            Assert.AreEqual(expectedGRCTPOFOF, actualGRCTPOFOF, 0.01d);

            double I = 1013.76;
            double IL = 1600;
            double UU = 50000;
            double UMAX = 80000;
            double expectedGAPOC = 81.1008;
            var actualGAPOC = ApproxMethods.GetApproxPowerOfConsumption(I, IL, UU, UMAX);
            Assert.AreEqual(expectedGAPOC, actualGAPOC, 0.01d);

            double KT = 1.4;
            double KE = 1.412;
            double kf_a = 0.8;
            double KPD = 0.9;
            double a_ag = 81.1008;
            double expectedGTPOC = 112.8108;
            var actualGTPOC = ApproxMethods.GetTotalPowOfConsumpt(UMAX, IL, KT, KE, kf_a, KPD, a_ag);
            Assert.AreEqual(expectedGTPOC, actualGTPOC, 0.01d);

            double expectedGNOA = 3;
            var actualGNOA = ApproxMethods.GetNumbOfAgg(NP);
            Assert.AreEqual(expectedGNOA, actualGNOA, 0.01d);
        }

        //[Test]
        //public void prostotest()
        //{
        //    double expected = 1;
        //    var actual = ElectricFilter.WpfApp.ReaultUC.FilterChoise();

        //}


    }

}