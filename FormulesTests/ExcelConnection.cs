﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop;
using Microsoft.Office.Interop.Excel;


namespace FormulesTests
{
    class ExcelConnection
    {
        public ExcelConnection(string path)
        {
            ExcelRead(path);
        }
        
        public double[] ExcelMediateData;              // Массив промежуточных значений

        public object[] ExcelFilterData;               // Массив значений электрофильтра

        public double[] ExcelParameterFilterData;      // Массив параметров электрофильтра

        public object[] ExcelSupplyUnitData;           // Массив значений агрегата питания

        public double[] ExcelParameterSupplyUnitData;  // Массив параметров агрегата питания

        public void ExcelRead(string filename)
        {
            // Подключение Microsoft Excel
            Application exp = new Application();
            Workbook expBook = exp.Workbooks.OpenXML(AppDomain.CurrentDomain.BaseDirectory + @"\" + filename);
            Worksheet expSheets;
            expSheets = (Worksheet)expBook.Sheets[1];

            // Выбор промежуточных значений
            Microsoft.Office.Interop.Excel.Range IntermediateResults = expSheets.UsedRange.Range[expSheets.Cells[26, 8], expSheets.Cells[27, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array IntermediateResultsArray = (Array)IntermediateResults.Cells.Value2;
            ExcelMediateData = IntermediateResultsArray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();

            // Выбор типа электрофильтра
            Microsoft.Office.Interop.Excel.Range FilterResults = expSheets.UsedRange.Range[expSheets.Cells[31, 8], expSheets.Cells[45, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array FilterResultsArray = (Array)FilterResults.Cells.Value2;
            ExcelFilterData = FilterResultsArray.Cast<object>().ToArray();

            // Параметры электрофильтра
            Microsoft.Office.Interop.Excel.Range FilterParametersResults = expSheets.UsedRange.Range[expSheets.Cells[49, 8], expSheets.Cells[80, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array FilterParametersArray = (Array)FilterParametersResults.Cells.Value2;
            ExcelParameterFilterData = FilterParametersArray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();

            // Выбор агрегата питания
            Microsoft.Office.Interop.Excel.Range PowerUnitResults = expSheets.UsedRange.Range[expSheets.Cells[84, 8], expSheets.Cells[92, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array PowerUnitArray = (Array)PowerUnitResults.Cells.Value2;
            ExcelSupplyUnitData = PowerUnitArray.Cast<object>().ToArray();

            // Параметры агрегата питания
            Microsoft.Office.Interop.Excel.Range PowerUnitParametersResults = expSheets.UsedRange.Range[expSheets.Cells[96, 8], expSheets.Cells[98, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array PowerUnitParametersArray = (Array)PowerUnitParametersResults.Cells.Value2;
            ExcelParameterSupplyUnitData = PowerUnitParametersArray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();

            exp.Workbooks.Close();
        }
    }
}
