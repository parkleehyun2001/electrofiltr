﻿using System;
using System.Collections.Generic;
using System.Text;
using FilterFormules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;
using static FilterFormules.Formules;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using CollectionAssert = Microsoft.VisualStudio.TestTools.UnitTesting.CollectionAssert;
using NUnit.Framework;

namespace FormulesTests
{
    public class Tests2
    {
        [Test]
        public void TestWasteAmount()
        {
            double vro = 108000;
            double x = 100;
            double tr = 300;
            double pbar = 101.5;
            double pgi = 9.5;
            double expected = 232602.60;

            var actual = ApproxMethods.GetWasteAmount(vro, x, tr, pbar, pgi);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestlocalSpace()
        {
            double v_gaz = 232602.60;
            double w = 0.8;
            double expected = 80.76;

            var actual = ApproxMethods.GetlocalSpace(v_gaz, w);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestUpdatedSpeed()
        {
            double v_gaz = 232602.60;
            double FA= 97.4;
            double expected = 0.66;

            var actual = ApproxMethods.GetUpdatedSpeed(v_gaz, FA);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestHurryPlane()
        {
            double NP = 3;
            double L = 3.84;
            double HP = 0.133;
            double WGE = 0.663365866;
            double expected = 130.57;

            var actual = ApproxMethods.GetHurryPlane(NP, L, HP, WGE);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestGasResidenceTime()
        {
            double L = 3.84;
            double WGE = 0.663365866;
            double expected = 5.78;

            var actual = ApproxMethods.GetGasResidenceTime(L, WGE);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestCriticalFieldOfNegativeElectrode()
        {
            double tr = 300;
            double pbar =101.5;
            double pgi = 9.5;
            double R = 0.002;
            double expected = 3113536.88;

            var actual = ApproxMethods.GetCriticalFieldOfNegativeElectrode(tr, pbar, pgi, R);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestCriticaVoltageOfElectrode()
        {
            double EKR = 3113536.885;
            double R = 0.002;
            double HK = 0.3;
            double HP =0.133;
            double expected = 27640.52;

            var actual = ApproxMethods.GetCriticaVoltageOfElectrode(EKR, R, HK, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestElectricFieldOflocalSpace()
        {
            double sr_tok = 0.22;
            double expected = 257034.78;

            var actual = ApproxMethods.GetElectricFieldOflocalSpace(sr_tok);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions1()
        {
            double EOC = 257034.7836;
            double spyli = 2;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "игольчатый";
            double expected = 0.0048;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions2()
        {
            double EOC = 257034.7836;
            double spyli = 10;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "игольчатый";
            double expected = 0.0108;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions3()
        {
            double EOC = 257034.7836;
            double spyli = 15;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "игольчатый";
            double expected = 0.0133;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions4()
        {
            double EOC = 257034.7836;
            double spyli = 25;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "игольчатый";
            double expected = 0.0172;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions5()
        {
            double EOC = 257034.7836;
            double spyli = 50;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "игольчатый";
            double expected = 0.0243;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalAnalyticalOfFractions6()
        {
            double EOC = 257034.7836;
            double spyli = 75;
            double sr_tok = 0.22;
            double WGE = 0.663365866;
            double vz_gaza = 0.019403791;
            string COR_EL = "игольчатый";
            double expected = 0.0297;

            var actual = ApproxMethods.GetPhysicalAnalyticalOfFractions(EOC, spyli, sr_tok, WGE, vz_gaza, COR_EL);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning1()
        {
            double NP = 3;
            double W_W = 0.004865029;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.47;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning2()
        {
            double NP = 3;
            double W_W = 0.010878535;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.75;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning3()
        {
            double NP = 3;
            double W_W = 0.01332343;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.82;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning4()
        {
            double NP = 3;
            double W_W = 0.017200475;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.89;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning5()
        {
            double NP = 3;
            double W_W = 0.024325145;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.95;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestFractionalNotchOfCleaning6()
        {
            double NP = 3;
            double W_W = 0.029792096;
            double L = 3.84;
            double WGE = 0.663365866;
            double HP = 0.133;
            double expected = 0.97;

            var actual = ApproxMethods.GetFractionalNotchOfCleaning(NP, W_W, L, WGE, HP);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestGeneralAnalyticalNotchOfCleaning()
        {
            List<double> Z_Z = new List<double>() { 0.470188044, 0.758388562, 0.824419489, 0.894166512, 0.958254586, 0.979554503 };
            double expected = 0.814;

            var actual = ApproxMethods.GetGeneralAnalyticalNotchOfCleaning(Z_Z);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestPhysicalSpeedOfParticles()
        {
            List<double> W_W = new List<double>() { 0.004865029, 0.010878535, 0.01332343, 0.017200475, 0.024325145, 0.029792036 };
            double expected = 0.0167;

            var actual = ApproxMethods.GetPhysicalSpeedOfParticles(W_W);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestAmountHurryOfDust()
        {
            double st_och_gaza = 0.814161949;
            double kpyli = 30;
            double v_gaza = 232602.6074;
            double expected = 5681.28;

            var actual = ApproxMethods.GetAmountHurryOfDust(st_och_gaza, kpyli, v_gaza);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestAmountRemovedOfDust()
        {
            double st_och_gaza = 0.814161949;
            double kpyli = 30;
            double v_gaza = 232602.6074;
            double expected = 1296.79;

            var actual = ApproxMethods.GetAmountRemovedOfDust(st_och_gaza, kpyli, v_gaza);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestRationalDustOfElectrodes()
        {
            double ud_sopr = 100000000;
            double expected = 1.064;

            var actual = ApproxMethods.GetRationalDustOfElectrodes(ud_sopr);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestRecTimeBtwShakeOfElectr()
        {
            double FOC = 8433;
            double pylemk = 1.064;
            double v_gaz = 232602.6074;
            double kpyli = 30;
            double st_och_gaza = 0.814161949;
            double expected = 0.0263;

            var actual = ApproxMethods.GetRecTimeBtwShakeOfElectr(FOC, pylemk, v_gaz, kpyli, st_och_gaza);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestReqCurToPowOneFieldOfFilter()
        {
            double sr_tok = 0.22;
            double H = 12;
            double L = 3.84;
            double NG= 30;
            double HK = 0.3;
            double expected = 1013.76;

            var actual = ApproxMethods.GetReqCurToPowOneFieldOfFilter(sr_tok, H, L, NG, HK);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestApproxPowerOfConsumption()
        {
            double I = 1013.76;
            double IL = 1600;
            double UU = 50000;
            double UMAX = 80000;
            double expected = 81.1008;

            var actual = ApproxMethods.GetApproxPowerOfConsumption(I, IL, UU, UMAX);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestTotalPowOfConsumpt()
        {
            double UMAX = 80000;
            double IL = 1600;
            double KT = 1.4;
            double KE = 1.412;
            double kf_a = 0.8;
            double KPD = 0.9;
            double a_ag = 81.1008;
            double expected = 112.8108;

            var actual = ApproxMethods.GetTotalPowOfConsumpt(UMAX, IL, KT, KE, kf_a, KPD, a_ag);
            Assert.AreEqual(expected, actual, 0.01d);
        }

        [Test]
        public void TestNumbOfAgg()
        {
            double NP = 3;
            double expected = 3;

            var actual = ApproxMethods.GetNumbOfAgg(NP);
            Assert.AreEqual(expected, actual, 0.01d);
        }
    }
}
