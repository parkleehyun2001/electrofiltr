using NUnit.Framework;
using FilterFormules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using static FilterFormules.Formules;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using CollectionAssert = Microsoft.VisualStudio.TestTools.UnitTesting.CollectionAssert;

namespace FormulesTests
{
    [TestClass]
    public class Tests
    {
        static string filename = "������.xlsx";   // ��� ����� Microsoft Excel
                
        ExcelConnection test = new ExcelConnection(filename);  // ��������� ������ ����������� � Miscrosoft Excel

        ApproxMethods AppMet = new ApproxMethods();   // ��������� ������ �������������� ���������� 

        // �������� ������
        
        static double vro = 108000;  // ������ ������ ���� �� ������� ��� ���������� ��������, �3/� 
        static double x = 100;     // ���������� ������� ����� � ����, �/�3
        static double tr = 300; // ����������� ���������� ���� �� �����, C
        static double pbar = 101.5; // ��������������� �������� �� ���������, ���
        static double pgi = 9.5;  // ���������� �������� ���������� ����, ���
        static double w = 0.8;     // ������������� �������� ���������� ����, �/c
        static double sr_tok = 0.22; // �������� ������� ��� ������
        static double kpyli = 30; // ������������ ���� �� �����
        static double vz_gaza = Math.Pow(2.2, -5); // ������������ �������� ���� ��� ������� ��������
        static double ud_sopr = 100000000; // �������� ������������� ������������� ���� ����
        static List<double> spyli = new List<double>() { 2, 10, 15, 25, 50, 75 }; // ����������� ������ ����
        
        Formules FormulesTest = new Formules(vro, x, tr, pbar, pgi, w, spyli, kpyli, vz_gaza, sr_tok, ud_sopr);

        
        
        // ������ ���������� ���� � ������� �������� (v_gaz)
        [TestMethod]
        public void TestWasteAmount()
        {
            double expected = test.ExcelMediateData[0];
            double result = FormulesTest.v_gaz;

            Assert.AreEqual(expected, result);
        }

        // ������� ��������� ������� �������������� (F_PA)
        [TestMethod]
        public void TestlocalSpace()
        {
            double expected = test.ExcelMediateData[1];
            double result = FormulesTest.F_PA;

            Assert.AreEqual(expected, result);
        }

        // ����� ���� �������������
        [TestMethod]
        public void TestSelectedFilter()
        {
            object[] FilterValues = {
                    FormulesTest.markFilter, // ����� ��������������
                    FormulesTest.NCK,        // ����� ������
                    FormulesTest.NG,         // ����� ������� ��������
                    FormulesTest.H,          // �������� ������ ����������
                    FormulesTest.L,          // �������� ����� ����
                    FormulesTest.NP,         // ����� �����
                    FormulesTest.FA,         // ������� ��������� �������
                    FormulesTest.HK,         // ��� ����� ����������� �����������
                    FormulesTest.FOC,        // ����� ������� ���������
                    FormulesTest.LE,         // ����� ��������������
                    FormulesTest.HE,         // ������ ��������������
                    FormulesTest.BE,         // ������ ��������������
                    FormulesTest.R,          // ����������� ������ ������������� ���������
                    FormulesTest.WGE,        // ���������� �������� ���� � ��������������
                    FormulesTest.COR_EL      // ��� ������������� ���������
                };

            var expected = test.ExcelFilterData;
            var result = FilterValues;

            CollectionAssert.AreEqual(expected, result);
        }

        // ���������� ����� ������������ � ������������ ����������� (HP)
        [TestMethod]
        public void TestDistBtwElectrodes()
        {
            double expected = test.ExcelParameterFilterData[0];
            double result = FormulesTest.HP;

            Assert.AreEqual(expected, result);
        }

        // �������� ������������ ����������� (FF)
        [TestMethod]
        public void TestHurryPlane()
        {

            double expected = test.ExcelParameterFilterData[1];
            double result = FormulesTest.FF;

            Assert.AreEqual(expected, result);
        }

        // ����� ���������� ���� � �������������� (TT)
        [TestMethod]
        public void TestGasResidenceTime()
        {
            double expected = test.ExcelParameterFilterData[2];
            double result = FormulesTest.TT;

            Assert.AreEqual(expected, result);
        }

        // ����������� ������������� �������������� ���� ��� ������������� ��������� ������������� ���������� (EKR)
        [TestMethod]
        public void TestCriticalFieldOfNegativeElectrode()
        {
            double expected = test.ExcelParameterFilterData[3];
            double result = FormulesTest.EKR;

            Assert.AreEqual(expected, result);
        }

        // ����������� ���������� �������������� ���� � ������������� ��������� (UKR)
        [TestMethod]
        public void TestCriticaVoltageOfElectrode()
        {
            double expected = test.ExcelParameterFilterData[4];
            double result = FormulesTest.UKR;

            Assert.AreEqual(expected, result);
        }

        // ������������� �������������� ���� ������ ����������� ��������� ���� (EOC)
        [TestMethod]
        public void TestElectricFieldOflocalSpace()
        {
            double expected = test.ExcelParameterFilterData[5];
            double result = FormulesTest.EOC;

            Assert.AreEqual(expected, result);
        }

        // ���������� �������� ������ ������ �� ������������� ������ ��� �������� ������� (W_W)
        [TestMethod]
        public void TestPhysicalVelocityOnAnalyticalOfFractions()
        {
            List<double> expected = new List<double>();

            for (int i = 6; i < 12; i++)
            {
                expected.Add(test.ExcelParameterFilterData[i]);
            }

            List<double> result = FormulesTest.W_W;

            CollectionAssert.AreEqual(expected, result);
        }

        // ����������� ������� ������� ���� (Z_Z)
        [TestMethod]
        public void TestFractionalNotchOfCleaning()
        {
            List<double> expected = new List<double>();

            for (int i = 12; i < 18; i++)
            {
                expected.Add(test.ExcelParameterFilterData[i]);
            }

            List<double> result = FormulesTest.Z_Z;

            CollectionAssert.AreEqual(expected, result);
        }

        // ����� ������������� ������� ������� ���� (st_och_gaza)
        [TestMethod]
        public void TestGeneralAnalyticalNotchOfCleaning()
        {
            double expected = test.ExcelParameterFilterData[18];
            double result = FormulesTest.st_och_gaza;

            Assert.AreEqual(expected, result);
        }

        // ���������� �������� ������ ������ (WE)
        [TestMethod]
        public void TestPhysicalSpeedOfParticles()
        {
            double expected = test.ExcelParameterFilterData[19];
            double result = FormulesTest.WE;

            Assert.AreEqual(expected, result);
        }

        // ���������� ���������� ���� (KOP)
        [TestMethod]
        public void TestAmountHurryOfDust()
        {
            double expected = test.ExcelParameterFilterData[20];

            // � ������ ������ ���������� ��������� ������� ������ � Microsoft Excel
            double result = Math.Round(FormulesTest.KOP, 11);

            Assert.AreEqual(expected, result);
        }

        // ���������� �������� �� ������� ���� (KUP)
        [TestMethod]
        public void TestAmountRemovedOfDust()
        {
            // � ������ ������ ���������� ��������� ��������� �� 1 ���������
            double expected = Math.Round(test.ExcelParameterFilterData[21], 11);
            double result = Math.Round(FormulesTest.KUP, 11);

            Assert.AreEqual(expected, result);
        }

        // ������������ ����������� ������������ ���������� (pylemk)
        [TestMethod]
        public void TestRationalDustOfElectrodes()
        {
            double expected = test.ExcelParameterFilterData[22];
            double result = FormulesTest.pylemk;

            Assert.AreEqual(expected, result);
        }

        // ������������� ����� ����� ��������� ������������� ������������ ���������� (tvstr)
        [TestMethod]
        public void TestRecTimeBtwShakeOfElectr()
        {
            double expected = test.ExcelParameterFilterData[23];
            double result = FormulesTest.tvstr;

            Assert.AreEqual(expected, result);
        }

        // ���� ����, ����������� �� ������� ������ ���� �������������� (I
        public void TestReqCurToPowOneFieldOfFilter()
        {
            double expected = test.ExcelParameterFilterData[24];
            double result = FormulesTest.I;

            Assert.AreEqual(expected, result);
        }

        // ����� �������� ��������
        [TestMethod]
        public void TestSelectPowerUnit()
        {
            object[] FilterValues =
            {
                    FormulesTest.PMark, // ����� ������� ��������
                    FormulesTest.UMAX,  // ������������ ���������� (������������)
                    FormulesTest.UU,    // ������������ ���������� (�������)
                    FormulesTest.IL,    // ������������ ��� 
                    FormulesTest.IP,    // ������������ ��� 
                    FormulesTest.UC,    // ���������� ���� 
                    FormulesTest.NC,    // ������������ �� ���� ��������
                    FormulesTest.KPD,   // ��� �������� �������
                    FormulesTest.kf_a   // ����������� ��������
                };

            var expected = test.ExcelSupplyUnitData;
            var result = FilterValues;

            CollectionAssert.AreEqual(expected, result);
        }

        // ��������� ������������ �������� �������� �� ������� ���� (a_ag)
        [TestMethod]
        public void TestApproxPowerOfConsumption()
        {
            double expected = test.ExcelParameterSupplyUnitData[0];
            double result = FormulesTest.a_ag;

            Assert.AreEqual(expected, result);
        }

        // ����� ��������, ������������ ��������� ������� (a_obsh)
        [TestMethod]
        public void TestTotalPowerOfConsumption()
        {
            double expected = test.ExcelParameterSupplyUnitData[1];
            double result = FormulesTest.a_obsh;

            Assert.AreEqual(expected, result);
        }

        // ���������� ��������� ������� �� ������������� (n_ag)
        [TestMethod]
        public void TestNumbOfAgg()
        {
            double expected = test.ExcelParameterSupplyUnitData[2];
            double result = FormulesTest.n_ag;

            Assert.AreEqual(expected, result);
        }
    }
}